(function ($) {

    var url;
    var input;
    var query;
    var inputTimer;
    var ajaxInstanse;

    $.fn.autocomplete = function (callback) //{{{
    {
        input = $(this);
        input.on("keyup", function () {
            var query = $(this).val();

            // cancel any previously-set timer
            if (inputTimer) {
                clearTimeout(inputTimer);
            }
            if (typeof ajaxInstanse == "object") {
                ajaxInstanse.abort();
            }

            inputTimer = setTimeout(function () {
                searchAutoComplete(query, callback);
            }, 500);
        });
    };


    function searchAutoComplete(query, callback) {

        var suggestions = [];
        if (query != '') {

            ajaxInstanse = $.ajax({
                url: "/search_ajax",
                data: {query: query},
                type: "POST",
                success: function (response) {
                    callback(response);
                    //fillSuggestions(response.suggestions, container);
                }
            });
        }
        return suggestions;
    }

    function fillSuggestions(suggestions, container) {
        $suggestionsContainer = container.container.suggestions;
        $autocompleteContainer = container.container.autocomplete;

        $suggestionsContainer.empty();
        suggestions.forEach(function (suggest) {
            var $suggestBlock = $("<div />", {
                'class': 'person_tile'
            });

            var $suggestImg = $("<div />", {
                'class': 'suggest_img'
            }).append($("<img />", {
                'src': suggest.photo
            }));

            var $suggestText = $("<div />", {
                'class': 'suggest_text'
            }).text(suggest.names);

            $suggestBlock.append($suggestImg);
            $suggestBlock.append($suggestText);
            $suggestBlock.data(suggest);

            $suggestBlock.on("click", function () {
                /*
                 var parentForm = $("#file_edit");

                 var url = parentForm.attr("action") + "/tag_person";

                 var $personTag = $("<div />", {
                 'class': 'person_tag'
                 });

                 var $personLink = $("<a />", {}).text(suggest.names);

                 var $personTagDeleteLink = $("<a />", {
                 'class': 'person_tag_delete ajaxlink',
                 }).html("&#10006;");

                 var $personTagUpdateLink = $("<a />", {
                 'class': 'person_tag_on',
                 }).html("&#9986;");


                 $personTag.append($personLink);
                 $personTag.append($personTagUpdateLink);
                 $personTag.append($personTagDeleteLink);

                 $.ajax({
                 url: url,
                 type: 'POST',
                 data: {person: suggest.id},
                 success: function (response) {
                 if (response.status) {
                 $personLink.attr("href", response.person_link);
                 $personTagDeleteLink.attr("href", response.tag_delete_link);
                 $personTagUpdateLink.attr("href", response.tag_update_link);
                 $autocompleteContainer.replaceWith($personTag);
                 }
                 }
                 });
                 */
            });

            $suggestionsContainer.append($suggestBlock);
        });
    }

}(jQuery));