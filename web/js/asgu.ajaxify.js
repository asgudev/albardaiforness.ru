$(document).on("submit", ".ajaxform", function (event) {
    event.stopPropagation();
    var form = $(this);
    var formData = new FormData(form[0]);
    var popup = form.parents(".popup_container").data("popup");
    $.ajax({
        url: form.attr("action"),
        data: formData,
        type: "POST",
        processData: false,
        contentType: false,
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            //Upload progress
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    console.log(percentComplete);
                    $(".progressbar").css("width", (percentComplete * 100) + "%");
                    $(".progressbar span").text((percentComplete * 100) + "%");
                }
            }, false);
            return xhr;
        },
        popup: popup,
        success: function (response) {
            if (response.status) {
                if (response.command !== undefined) {
                    eval(response.command);

                }
            }
        }
    });
    return false;
});

$(document).on("click", ".ajaxlink", function () {
    var link = $(this);
    var $popup;
    if (link.parents(".popup_container").length) {
        $popup = link.parents(".popup_container").data("popup");
    }
    $.ajax({
        url: link.attr("href") + "?ajax",
        type: "GET",
        link: link,
        popup: $popup,
        success: function (response, val2, val3) {
            if (typeof response == "object") {
                if (response.command !== undefined) {
                    eval(response.command);
                }
            } else {
                showPopup(response);
            }
        }
    });

    return false;
});

$(document).on("click", ".popuplink", function () {
    var target = $(this).attr("href");
    var content = $(target).html();
    showPopup(content);
    return false;
});


function showPopup(content) {
    var overlay = $("<div />", {
        "class": "overlay"
    });
    $("body").append(overlay.show());

    var popup_container = setPopup(content);

    var popup = {
        container: popup_container,
        overlay: overlay
    };
    popup_container.data("popup", popup);
    overlay.on("click", function () {
        closePopup(popup);
    });
}

function reloadBlock(block, callback, url) {
    if (url == undefined) {
        url = location.pathname;
    }
    $.ajax({
        url: url,
        type: "GET",
        success: function (response) {
            $(block).replaceWith($(response).find(block));
            if (callback !== undefined) {
                callback();
            }
        }
    });
}

function setPopup(content) {
    var popup_container = $("<div />", {
        "class": "popup_container"
    });
    popup_container.html(content).show();
    $("body").append(popup_container.show()).scrollTop(0);

    return popup_container;
}

function closePopup(popup) {
    popup.overlay.remove();
    popup.container.remove();
}

