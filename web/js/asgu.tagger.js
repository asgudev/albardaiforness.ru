var crop;
var coords = {};
var person = {};
var inputTimer;

$(document).on('click', ".tag_person", function () {
    var $btn = $(this);
    var $autocompleteContainer = $("<div />", {
        'class': 'autocomplete_container'
    });
    var $input = $("<input />", {
        'placeholder': "Person ID"
    });
    var $suggestionsContainer = $("<div />", {
        'class': 'suggestions_container'
    });
    $btn.before($autocompleteContainer.append($input));
    $btn.before($autocompleteContainer.append($suggestionsContainer));
    $input.on("keyup", function () {
        var query = $(this).val();
        var container = {
            container: {
                suggestions: $suggestionsContainer,
                autocomplete: $autocompleteContainer
            }
        };

        // cancel any previously-set timer
        if (inputTimer) {
            clearTimeout(inputTimer);
        }

        inputTimer = setTimeout(function () {
            searchAutoComplete(query, container);
        }, 500);
    });


    return false;
});


function searchAutoComplete(query, container) {
    var suggestions = [];
    if (query != '') {

        $.ajax({
            url: "/search_ajax",
            data: {query: query},
            type: "POST",
            success: function (response) {
                fillSuggestions(response.suggestions, container);
            }
        });
    }
    return suggestions;
}

function fillSuggestions(suggestions, container) {
    $suggestionsContainer = container.container.suggestions;
    $autocompleteContainer = container.container.autocomplete;

    $suggestionsContainer.empty();
    suggestions.forEach(function (suggest) {
        var $suggestBlock = $("<div />", {
            'class': 'suggest'
        });

        var $suggestImg = $("<div />", {
            'class': 'suggest_img'
        }).append($("<img />", {
            'src': suggest.photo
        }));

        var $suggestText = $("<div />", {
            'class': 'suggest_text'
        }).text(suggest.names);

        $suggestBlock.append($suggestImg);
        $suggestBlock.append($suggestText);
        $suggestBlock.data(suggest);

        $suggestBlock.on("click", function () {
            var parentForm = $("#file_edit");
            var url = parentForm.attr("action") + "/tag_person";

            var $personTag = $("<div />", {
                'class': 'person_tag'
            });

            var $personLink = $("<a />", {}).text(suggest.names);

            var $personTagDeleteLink = $("<a />", {
                'class': 'person_tag_delete ajaxlink',
            }).html("&#10006;");

            var $personTagUpdateLink = $("<a />", {
                'class': 'person_tag_on',
            }).html("&#9986;");


            $personTag.append($personLink);
            $personTag.append($personTagUpdateLink);
            $personTag.append($personTagDeleteLink);

            $.ajax({
                url: url,
                type: 'POST',
                data: {person: suggest.id},
                success: function (response) {
                    if (response.status) {
                        $personLink.attr("href", response.person_link);
                        $personTagDeleteLink.attr("href", response.tag_delete_link);
                        $personTagUpdateLink.attr("href", response.tag_update_link);
                        $autocompleteContainer.replaceWith($personTag);
                    }
                }
            });
        });

        $suggestionsContainer.append($suggestBlock);
    });
}

$(document).on('click', ".person_tag_on", function () {
    var $link = $(this);
    person = {
        id: $link.data("id"),
        names: $link.data("names"),
        url: $link.attr("href"),
        selector: $link,
    };

    if ((typeof crop == "object") && (crop != null)) {
        crop.destroy();
    }
    crop = $.Jcrop('.preview img', {
        onSelect: markPerson,
        shade: true,
    });

    return false;
});


function markPerson(c) {
    $(".person-input").remove();
    var image = $(".preview img")[0];

    coords.displayWidth = $(".preview img").width();
    coords.naturalWidth = image.naturalWidth;
    coords.naturalHeight = image.naturalHeight;

    var module = coords.naturalWidth / coords.displayWidth;

    coords.x1 = (c.x * module).toFixed();
    coords.x2 = (c.x2 * module).toFixed();
    coords.y1 = (c.y * module).toFixed();
    coords.y2 = (c.y2 * module).toFixed();


    var personForm = personTagForm(coords);

    var $frame = $("#jcrop-frame");

    $(".jcrop-holder").append(
        personForm.css({
            'top': $frame.css('top'),
            'left': (parseInt($frame.css('left')) + parseInt($frame.css('width')))
        })
    );
}

function personTagForm(coords) {
    var $formContainer = $("<div />", {
        'class': "person-input",
    });

    var $names = $("<div />", {}).text(person.names);

    $formContainer.append($names);

    $buttonSubmit = $('<button />', {}).text("{{ 'Apply'|trans }}");
    $buttonCancel = $('<button />', {}).text("{{ 'Cancel'|trans }}");

    $buttonSubmit.on("click", function (event) {
        event.stopPropagation();
        $.ajax({
            url: person.url,
            type: "POST",
            data: {
                coordinates: coords,
            },
            success: function (response) {
                if (typeof response == "object") {
                    if (response.status) {
                        var personTagContainer = person.selector.parents(".person_tag");
                        personTagContainer.data("coordinates", coords);
                        $formContainer.remove();
                        person.selector.remove();
                        crop.destroy();
                        crop = null;
                    }
                }
            }
        });
        return false;
    });
    $buttonCancel.on("click", function () {
        crop.destroy();
        crop = null;
    });

    $formContainer.append($buttonSubmit);
    $formContainer.append($buttonCancel);

    return $formContainer;
}