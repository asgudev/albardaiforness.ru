$(document).on("keydown", function (e) {
    if (e.which === 8 && !$(e.target).is("input, textarea")) {
        e.preventDefault();
    }
});


$(document).on("mouseover", ".person_tag", function () {
    var personTag = $(this);
    var coordinates = personTag.data("coordinates");

    if ((coordinates !== undefined) && ((typeof crop === undefined) || (typeof crop != "object") || (crop == null))) {
        var $img = $(".preview img");
        var $imgClone = $img.clone();
        var $frameContainer = $("<div />", {
            'class': 'frame_container',
        }).css({
            'width': $img.width()
        });
        var $frame = $("<div />", {
            'class': 'frame',
        });


        var displayWidth = $img.width();
        var module = displayWidth / coordinates.naturalWidth;
        $frame.css({
            "left": coordinates.x1 * module,
            "top": coordinates.y1 * module,
            "width": (coordinates.x2 - coordinates.x1) * module,
            "height": (coordinates.y2 - coordinates.y1) * module
        });

        $imgClone.css({
            "left": -coordinates.x1 * module,
            "top": -coordinates.y1 * module,
            "width": $img.width(),
        });

        $frameContainer.append($frame.append($imgClone));

        $img.after($frameContainer);

        $(this).on("mouseout", function () {
            $frameContainer.remove();
        });
    }
});

$(".short-search").click(function () {
    $("#search-link, #short-form").toggleClass("hide");
    return false;
});

function centerBlock(block) {
    block.css("top", ($(window).height() - block.height()) / 2)
}

$('#apply_language').click(function () {
    $.ajax({
        url: "{{path('translation_apply')}}",
        type: "POST",
        success: function (e) {
            alert(e);
        }
    });
});

$("#byear").change(function () {
    birth = $("#birth").text().split("/");
    $("#birth").text(birth[0] + "/" + birth[1] + "/" + $("#byear").text());
});

centerBlock($(".center-block"));

$(window).resize(function () {
    centerBlock($(".center-block"));
});

$(".person-next").click(function () {
    owl.trigger('owl.next');
});
$(".person-prev").click(function () {
    owl.trigger('owl.prev');
});

$('#site-nav').hide();

$('a#mobile-menu-btn').click(function () {
    $('#site-nav').slideToggle('fast');
    $('a#mobile-menu-btn').toggleClass('menu-btn-open');
});
/*
 $(document).on("DOMSubtreeModified", function () {
 centerImages();
 });
 */

function centerImages() {
    $(".person_tile .photo").each(function () {
        var $container = $(this);
        var $img = $container.find("img");
        centerImage($container, $img)

        /*
         var $img = $("<img />").attr("src", $container.data("src"));
         $container.append($img);
         */

        $img.on("load", function () {
            centerImage($container, $img)
        });

        $img.on("error", function () {
            if ($container.data("gender") == 0) {
                $(this).attr("src", "/media/cache/relative/uploads/female.png");
            } else {
                $(this).attr("src", "/media/cache/relative/uploads/male.png");
            }
            $img.on("load", function () {
                centerImage($container, $img)
            });
        });
        function centerImage($container, $img) {
            $img.css({
                'margin-left': (($container.width() - $img[0].naturalWidth) / 2),
            });
        }
    });
}