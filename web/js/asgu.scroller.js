(function ($) {

    var $container;
    var $options;
    var $page = 1;
    var $ajax;

    $.fn.scroller = function (options, callback) {
        $options = options;
        $container = $(this);
        var $loading = $("<div />", {
            'class': 'loader'
        });

        $(window).on('scroll', function () {
            if ($(document).height() - $(window).height() == $(window).scrollTop()) {
                loadData();
            }
        });

        if ($container.height() < $(window).height()) {
            loadData();
        }

        function loadData() {
            $container.append($loading);
            if ((typeof $ajax != "object") || ($ajax === '')) {
                $ajax = $.ajax({
                    url: $options.url,
                    type: "GET",
                    data: {
                        ajax: true,
                        page: $page,
                    },
                    dataType: 'html',
                    success: function (html) {
                        $container.append(html);
                        $page += 1;
                        $loading.remove();
                        $ajax = '';
                    }
                });
            }
        }
    }


}(jQuery));