var Efa = {};
var Efo;
var Ewr;
var Ewp = null;
var Ead;
var Eve;
var Ezf, Esd = null;
var Ebn = false;
var Emn = false;
var Esp = true;
var Eeq = [];
var Ess = "";
var Eis = "";
var Eec = null;
var Epc = null;
var staticModeAfterRead;
var lastHash, currentHash;
var Elb = null;
var Ebi;
var Esb;
var Elh = null;
var Eec = 0;
var Epc = viewperson;
var Esc = true;

function PL() {
	if ((typeof(Ajax) != "undefined") && Ajax.getTransport()) {
		window.onmousewheel = EPU;
		var c = GC("zoomfactor");
		var zf = parseFloat((c === null) ? defaultZoom : c);
			Ezf = ((zf >= 0.25) && (zf <= 2)) ? zf : 1.25;
		
		var c = GC("showmiddlename");
			Emn = parseInt((c === null) ? defaultMiddleName : c) ? true : false;
		
		var c = GC("showdetail");
			Esd = (c === null) ? defaultDetail : c;
			self.navframe.NSD(Esd);
		
		var c = GC("showcousins");
			self.navframe.NSC((c === null) ? defaultCousins : c);
		
		var c = GC("showchildren");
			self.navframe.NSH((c === null) ? defaultChildren : c);
		
		var c = GC("showparents");
			self.navframe.NSA((c === null) ? defaultParents : c);
			Ebi = (document.all && (navigator.userAgent.toLowerCase().indexOf("msie") >= 0));
			Esb = (navigator.userAgent.toLowerCase().indexOf("safari") >= 0);
			
		Ewr = true;
		Ead = true;
		Efo = person;
		
		EFR(data);
		
	} else {
		SS("treeframe", false);
		SS("noajax", true);
	}
};
function EPU(e){
	
	if (e.wheelDelta > 0){
		ECZ(true);
	}else{
		ECZ(false);
	}
	
}




function EBT() {
	if (!Esb) {
		var h = new String(window.location.hash);
		if (h.length && (h.charAt(0) == "#")) {
			h = h.substring(1);
		}
		if (Elh && (Elh != h)) {
			return;
		}
		var a = h.split(":");
		var m = a[0];
		var i = a[1];
		lastHash = currentHash;
		currentHash = m;
		if ((i && (i != viewperson)) || (m && (m != "view"))) {
			if ((Eec !== null) && (i == Epc) && (m === "view")) {
				EFE(false);
			} else {
				EUS(false, null, null, true, true);
			}
		}
	}
};

function EFR(json) {
	ERS(json.t);
	Ess = json.t;
	Eve = json.v;
	Ewr = json.aw;
	Ewp = json.pw ? person : null;
	Ead = json.aa;
	Efo = json.fp;
	
	ERP();
};

function ERP() {
	if (Esd === null) {
		Esd = "";
		for (var j in Efa) {
			if (Efa[j].r) {
				Esp = true;
			}
		}
		self.navframe.NSD(Esd);
	}
	EUS(true, null, "view", true, false);
	EUL(false);

	setInterval(EBT, 250);
};

function EUS(r, i, m, d, s, _5) {
	var pi = Evp = viewperson;
	var pm = viewMode = "view";
	if (r) {
		var ap = person;
		if (Efo && !Efa[Efo]) {
			Efa[Efo] = {};
		}
		FRF(Efa, ap, Efo);
		if (ap && Efa[ap]) {
			self.navframe.NSP(ap);
		} else {
			self.navframe.NSP(Efo);
		}
		var fc = 0;
		for (var j in Efa) {
			fc++;
		}
		self.navframe.NCP(fc);
	}
	if (i) {
		Evp = i;
	}
	if (m) {
		viewMode = m;
	}
	viewperson = Evp;
	if ((!Evp) || (!Efa[Evp])) {
		if (Efo && Efa[Efo]) {
			Evp = Efo;
		} else {
			for (Evp in Efa) {
				break;
			}
		}
	}
	
	if (d || (Evp != pi)) {
		self.treeframe.TRT(Efa, Evp, person, Esd, Ebn, Emn, Esp, self.navframe.NGH(), self.navframe.NGA(), self.navframe.NGC(), pi, Ezf, s);
		self.navframe.NRT();
	}
	if (r || (Evp != pi) || true) {
		if (parent && parent.postMessage) {
			parent.postMessage("focus=" + Evp, "*");
		}
	}
};


function ERF() {
	EUS(false, null, null, true, true);
};

function ESP(i, s) {
	for (var j = 0; j < (Eeq.length - 1); j++) {
		if (Eeq[j] == i) {
			Eeq.splice(j, 1);
			EUS(false, i, "edit", false, s);
			return;
		}
	}
	var vm = "view";
	Eeq = [];
	EUS(false, i, "view", false, s);
};


function EFV(i, p, v) {
	if (i) {
		Efa[i] = Efa[i] || {};
		if ((p == "x") || (p == "s")) {
			if (Efa[i].s && Efa[Efa[i].s]) {
				Efa[Efa[i].s].s = null;
			}
		}
		if (p == "x") {
			delete Efa[i];
		} else {
			if ((p == "s") && v) {
				Efa[v] = Efa[v] || {};
				if (Efa[v].s && Efa[Efa[v].s]) {
					Efa[Efa[v].s].s = null;
				}
				Efa[v].s = i;
			}
			Efa[i][p] = v ? v : null;
		}
	}
};

function EPV(i1, i2, p, v) {
	if (i1 && i2) {
		Efa[i1] = Efa[i1] || {};
		Efa[i2] = Efa[i2] || {};
		var fn = p + "p";
		Efa[i1][fn] = Efa[i1][fn] || {};
		Efa[i2][fn] = Efa[i2][fn] || {};
		Efa[i1][fn][i2] = v.length ? v : null;
		Efa[i2][fn][i1] = v.length ? v : null;
	}
};

function ERS(s) {
	var c = ECL(s);
	for (var j = 0; j < c.length; j++) {
		var e = c[j];
		var i = e.t.substring(1, e.t.length);
		var v = e.v.replace(/\\t/g, "\t").replace(/\\n/g, "\n").replace(/\\\\/g, "\\");
		if (e.t.charAt(0) == "i") {
			EFV(i, e.p, v);
		} else {
			if (e.t.charAt(0) == "p") {
				var ii = i.split(" ");
				EPV(ii[0], ii[1], e.p, v);
			}
		}
	}
};

function ECL(s) {
	var l = NE(s).split("\n");
	var c = [];
	for (var j = 0; j < l.length; j++) {
		var e = l[j].split("\t");
		for (var k = 1; k < e.length; k++) {
			c[c.length] = {
				t: e[0],
				p: e[k].charAt(0),
				v: e[k].substring(1, e[k].length)
			};
		}
	}
	return c;
};

function EOS(s) {
	var c = ECL(s);
	var os = "";
	var pi = null;
	var pc = [];
	for (var j = 0; j < c.length; j++) {
		var e = c[j];
		if (e.t != pi) {
			if (pi) {
				os += pi + "\t" + pc.join("\t") + "\n";
			}
			pi = e.t;
			pc = [];
		}
		var pl = pc.length;
		pc[((pl > 0) && (pc[pl - 1].charAt(0) == e.p)) ? (pl - 1) : pl] = e.p + e.v;
	}
	if (pi) {
		os += pi + "\t" + pc.join("\t") + "\n";
	}
	return os;
};

function EFC(i, c) {
	for (var p in c) {
		var v = c[p] ? NE(new String(c[p])) : "";
		EFV(i, p, v);
		GetElement("newscript").value += "\ni" + i + "\t" + p.charAt(0) + v.replace(/\\/g, "\\\\").replace(/\n/g, "\\n").replace(/\t/g, "\\t");
	}
	EUL(false);
};

function EPC(i1, i2, c) {
	for (var p in c) {
		var v = c[p] ? NE(new String(c[p])) : "";
		EPV(i1, i2, p, v);
		GetElement("newscript").value += "\np" + i1 + " " + i2 + "\t" + p.charAt(0) + v.replace(/\\/g, "\\\\").replace(/\n/g, "\\n").replace(/\t/g, "\\t");
	}
	EUL(false);
};

function ESR(_b, _c, _d) {
	Edf = false;
	if (_d.ok) {
		Eve = _d.v;
		Ess += "\n" + Eis;
		Eis = "";
		var ns = "";
		Ess += ns.substring(0, _c);
		if (_d.t) {
			Efa = {};
			ERS(_d.t);
			Ess = _d.t;
			ERS("");
			EUS(true, null, null, true, false);
		}
	} else {
		RE("The family could not be saved - please try again");
	}
	EUL(true);
};

function EUL(js) {
	p = js ? "lsaved" : "linitial";
};

function EAR(_f, _10, _11) {
	if (_11.ok) {
		ST("lfamilyname", _11.n);
		SS("addfamily", false);
	} else {
		RE(_11.er || "The family could not be added");
	}
};

function EBS() {
	var ap = person;
	ESP((ap && Efa[ap]) ? ap : Efo, true);
};

function ECZ(zi) {
	if (zi && (Ezf < 2)) {
		Ezf += 0.25;
	} else {
		if ((!zi) && (Ezf > 0.5)) {
			Ezf -= 0.25;
		}
	}
	SC("zoomfactor", Ezf);
	ERF();
};

function ECD(d, bn, mn, sp) {
	if (d !== null) {
		SC("showdetail", d);
		Esd = d;
	}
	if (bn !== null) {
		SC("showbirthname", bn ? 1 : 0);
		Ebn = bn;
	}
	if (mn !== null) {
		SC("showmiddlename", mn ? 1 : 0);
		Emn = mn;
	}
	if (sp !== null) {
		SC("showphoto", sp ? 1 : 0);
		Esp = sp;
	}
	self.navframe.NSD(Esd);
	ERF();
};

function ECO() {
	SC("showcousins", self.navframe.NGC());
	ERF();
};

function ECH() {
	SC("showchildren", self.navframe.NGH());
	ERF();
};

function ECP() {
	SC("showparents", self.navframe.NGH());
	ERF();
};