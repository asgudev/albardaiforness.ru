function GetElement(element) {
	return document.getElementById(element);
};

function SetValue(element, value) {
	GetElement(element).value = value ? value : "";
};

function GetValue(element) {
	return GetElement(element).value;
};

function SO(e, v) {
	var s = GetElement(e);
	var v = v ? v : "";
	for (var j = 0; j < s.options.length; j++) {
		if (s.options[j].value == v) {
			s.selectedIndex = j;
		}
	}
};

function GO(e) {
	var v = GetElement(e);
	return v.options[v.selectedIndex].value;
};

function SS(e, s) {
	GetElement(e).style.display = s ? "inline" : "none";
};

function GS(e) {
	return GetElement(e).style.display != "none";
};

function SI(e, v) {
	GetElement(e).style.visibility = v ? "visible" : "hidden";
};

function GI(e) {
	return GetElement(e).style.visibility != "hidden";
};

function FS(e) {
	GetElement(e).focus();
	GetElement(e).select();
};

function SR(e, s) {
	GetElement(e).className = s ? "showrows" : "hiderows";
};

function SetHtml(e, h) {
	GetElement(e).innerHTML = h;
};

function ST(e, t) {
	GetElement(e).innerHTML = EH(t);
};

function NE(s) {
	return s.replace(/\r\n?/g, "\n");
};

function EH(v) {
	return v.replace(/&/g, "&amp;").replace(/"/g, "&quot;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
};

function EL(v) {
	return v ? EH(v.replace(/\n/g, "^$")).replace(/\^\$/g, "<BR>") : "";
};

function DT() {
	return (new Date()).getTime();
};

function AG( person, callback) {
	new Ajax.Request("/person/" + person + '/fstree/', {
		method: "post",
		onComplete: function(response) {
			callback(((response.status == 200) && response.responseText) ? eval("(" + response.responseText + ")") : {});
		}
	});
};

var Bw = null;

function CE(w) {
	Bw = w;
	window.onerror = SE;
};

function TR() {
	var s = "";
	for (var a = TR; a; a = a.caller) {
		s += (a.name || a.toString().match(/function (\w*)/)) + "<";
		if (a.caller == a) {
			break;
		}
	}
	return s;
};

function SE(m, u, l, w) {
	w = w || window;
	/*if (Bw) {
		if (Bw.SE) {
			Bw.SE(m, u, l, w);
		}
	} else {
		AP("log_js_error", {}, m + "|" + (w ? w.location : "") + "|" + u + "|" + l + "|", function() {}, null);
	}*/
};

function RE(e) {
	alert(e);
};

function SC(n, v) {
	var d = new Date();
	d.setTime(d.getTime() + 365 * 86400000);
	document.cookie = n + "=" + v + "; expires=" + d.toGMTString() + "; path=/";
};

function GC(n) {
	var cs = document.cookie.split(";");
	for (var j = 0; j < cs.length; j++) {
		var c = cs[j];
		while (c.charAt(0) == " ") {
			c = c.substring(1, c.length);
		}
		if (c.substring(0, n.length + 1) == (n + "=")) {
			return c.substring(n.length + 1, c.length);
		}
	}
	return null;
};

function UL(l) {
	var dw = self.screen.width,
		dh = self.screen.height;
	if (top.innerHeight) {
		dw = top.innerWidth;
		dh = top.innerHeight;
	} else {
		if (top.document.documentElement && top.document.documentElement.clientHeight) {
			dw = top.document.documentElement.clientWidth;
			dh = top.document.documentElement.clientHeight;
		} else {
			if (top.document.body) {
				dw = top.document.body.clientWidth;
				dh = top.document.body.clientHeight;
			}
		}
	}
	var w = window.open(l.href, "uplink", "toolbar=1,location=1,status=1,menubar=1,scrollbars=1,resizable=1," + "width=" + (dw - 64) + ",height=" + (dh - 64));
	if (w) {
		w.focus();
	}
	return !w;
};