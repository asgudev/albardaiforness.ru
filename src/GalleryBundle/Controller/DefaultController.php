<?php

namespace GalleryBundle\Controller;

use FamilyTreeBundle\Controller\BaseController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class DefaultController
 * @Route("/gallery")
 */
class DefaultController extends BaseController
{
    /**
     * @Route("/", name="gallery_index")
     */
    public function indexAction()
    {
        $images = $this->getDoctrine()->getRepository("GalleryBundle:File")->findGallery();
        //$images = [];
        return $this->render("@Gallery/Default/index.html.twig", [
            'images' => $images
        ]);
    }
}
