<?php

namespace GalleryBundle\Controller;

use FamilyTreeBundle\Entity\PersonTag;
use GalleryBundle\Form\FileEditType;
use GalleryBundle\Form\FileUploadType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use GalleryBundle\Entity\File;
use Symfony\Component\HttpFoundation\Response;


class FilesController extends Controller
{
    /**
     * @Route("/gallery/{id}", name="gallery_show_file")
     */
    public function showFileAction(Request $request, File $file)
    {
        if (!$file) {
            die("123");
        }

        $twig = $this->get('twig');
        $template = $twig->loadTemplate("@Gallery/Files/showFile.html.twig");



        if ($request->get("ajax") !== null) {
            $twig = $this->get('twig');
            $template = $twig->loadTemplate("@Gallery/Files/showFile.html.twig");

            return new Response($template->renderBlock("body",[
                'file' => $file,
            ]));
        } else {
            return $this->render("@Gallery/Files/showFile.html.twig", [
                'file' => $file,
            ]);
        }
    }

    /**
     * @Route("/admin/gallery/upload", name="gallery_upload_file")
     */
    public function fileUploadAction(Request $request)
    {
        $file = new File();
        $form = $this->createForm(FileUploadType::class, $file, [
            'action' => $this->generateUrl('gallery_upload_file', [

            ]),
            'source' => 'gallery'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($file);
                $em->flush();


                $editform = $this->createForm(FileEditType::class, $file, [
                    'action' => $this->generateUrl('person_edit_file', [
                        'id' => $file->getId()
                    ]),
                    'source' => 'gallery'
                ]);

                $editpage = $this->renderView("@Gallery/Files/editFile.html.twig", [
                    'form' => $editform->createView(),
                    'file' => $file
                ]);

                $data = [
                    'status' => true,
                    'file' => [
                        'path' => $file->getFile(),
                        'title' => $file->getTitle(),
                        'type' => $file->getType(),
                    ],
                    'form' => $editpage,
                    'command' => "this.popup.container.html(response.form);",
                ];

                return new JsonResponse($data);
            } else {
                $data = [
                    'status' => false,
                    'error' => $form->getErrors()
                ];
                return new JsonResponse($data);
            }
        }


        return $this->render("@Gallery/Files/uploadFile.html.twig", [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/file/{id}/edit/tag_person", name="file_mark_person")
     */
    public function editMarkPerson(Request $request, File $file)
    {
        $em = $this->getDoctrine()->getManager();
        $request = $request->request->all();
        $person = $this->getDoctrine()->getRepository("TreeBundle:Person")->find($request['person']);

        if ((!$person) || (!$file)) {
            die("entity not found");
        }

        $personTag = new PersonTag($file, $person);

        if (array_key_exists("coordinates", $request)) {
            $coords = $request['coordinates'];
            $personTag->setMeta($coords);
        }

        $em->persist($personTag);
        $em->flush();

        $data = [
            'status' => true,
            'tag_delete_link' => $this->generateUrl("person_tag_delete", [
                'id' => $personTag->getId()
            ]),
            'tag_update_link' => $this->generateUrl("person_tag_update", [
                'id' => $personTag->getId()
            ]),
            'person_link' => $this->generateUrl("person_show", [
                'id' => $personTag->getPerson()->getId()
            ]),
        ];

        return new JsonResponse($data);
    }

    /**
     * @Route("/admin/file/crop", name="file_image_crop")
     */
    public function cropImageFile(Request $request)
    {
        $cropper = $this->get("gallery.cropper");

        $src = $request->get("avatar_src");
        $data = $request->get("avatar_data");
        //$file = ->get("avatar_file");

        dump($cropper);
        dump($src);
        dump($data);
        dump($request->files);
        die();

        $em = $this->getDoctrine()->getManager();
        $request = $request->request->all();
        $person = $this->getDoctrine()->getRepository("TreeBundle:Person")->find($request['person']);

        if ((!$person) || (!$file)) {
            die("entity not found");
        }

        $personTag = new PersonTag($file, $person);

        if (array_key_exists("coordinates", $request)) {
            $coords = $request['coordinates'];
            $personTag->setMeta($coords);
        }

        $em->persist($personTag);
        $em->flush();

        $data = [
            'status' => true,
            'tag_delete_link' => $this->generateUrl("person_tag_delete", [
                'id' => $personTag->getId()
            ]),
            'tag_update_link' => $this->generateUrl("person_tag_update", [
                'id' => $personTag->getId()
            ]),
            'person_link' => $this->generateUrl("person_show", [
                'id' => $personTag->getPerson()->getId()
            ]),
        ];

        return new JsonResponse($data);
    }

    /**
     * @Route("/admin/gallery/{id}/edit", name="gallery_edit_file")
     */
    public function fileEditAction(Request $request, File $file)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(FileEditType::class, $file, [
            'action' => $this->generateUrl('person_edit_file', [
                'id' => $file->getId()
            ]),
            'source' => 'gallery'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                if ($form->get('delete')->isClicked()) {

                    $data = [
                        'status' => true,
                        'command' => 'closePopup(this.popup);$(".file_' . $file->getId() . '").remove();',
                    ];

                    $em->remove($file);
                    $em->flush();

                    return new JsonResponse($data);
                } elseif ($form->get('submit')->isClicked()) {
                    $em->persist($file);
                    $em->flush();
                    $data = [
                        'status' => true,
                        'file' => [
                            'path' => $file->getFile(),
                            'title' => $file->getTitle(),
                            'type' => $file->getType(),
                        ],
                        'command' => "closePopup(this.popup);",
                    ];
                    return new JsonResponse($data);
                }
            } else {
                $data = [
                    'status' => false,
                    'error' => $form->getErrors()
                ];
                return new JsonResponse($data);
            }
        }


        return $this->render("@Gallery/Files/editFile.html.twig", [
            'form' => $form->createView(),
            'file' => $file
        ]);
    }

    /**
     * @Route("/admin/persontag/{id}/delete", name="person_tag_delete")
     */
    public function deletePersonTag(Request $request, PersonTag $personTag)
    {
        if (!$personTag) {
            die("error");
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($personTag);
        $em->flush();

        $data = [
            'status' => true,
            'command' => 'link.parents(".person_tag").remove();'
        ];

        return new JsonResponse($data);
    }

    /**
     * @Route("/admin/persontag/{id}/update", name="person_tag_update")
     */
    public function updatePersonTagMeta(Request $request, PersonTag $personTag)
    {
        if (!$personTag) {
            die("error");
        }
        $em = $this->getDoctrine()->getManager();

        if ($request->get("coordinates") !== null) {
            $personTag->setMeta($request->get("coordinates"));
        }

        $em->flush();


        $data = [
            'status' => true,
        ];

        return new JsonResponse($data);
    }
}