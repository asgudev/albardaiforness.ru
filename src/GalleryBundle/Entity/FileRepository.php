<?php
/**
 * Created by IntelliJ IDEA.
 * User: iProtoss
 * Date: 15.05.2016
 * Time: 12:53
 */

namespace GalleryBundle\Entity;


use Doctrine\ORM\EntityRepository;
use FamilyTreeBundle\Entity\Person;

class FileRepository extends EntityRepository
{

    public function find($id)
    {

        $file = $this->createQueryBuilder('f')
            ->addSelect('ft')
            ->addSelect('fo')
            ->addSelect('ftp')
            ->leftJoin('f.personTags', 'ft')
            ->leftJoin('f.owner', 'fo')
            ->leftJoin('ft.person', 'ftp')
            ->where('f.id = :id')
            ->setParameters([
                'id' => $id
            ])
            ->getQuery()
            ->getSingleResult();
        return $file;
    }

    public function findTagged(Person $person)
    {
        $files = $this->createQueryBuilder('f')
            ->addSelect('ft')
            ->addSelect('fo')
            ->addSelect('ftp')
            ->innerJoin('f.personTags', 'ft')
            ->innerJoin('f.owner', 'fo')
            ->innerJoin('ft.person', 'ftp')
            ->where('ftp = :person')
            ->setParameters([
                'person' => $person
            ])
            ->getQuery()
            ->getResult();
        return $files;
    }

    public function findBackground()
    {
        return $this->createQueryBuilder('f')
            ->where('f.isBackground = true')
            ->setMaxResults(1)
            ->orderBy('RAND()')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findGallery($limit = 100)
    {
        return $this->createQueryBuilder('g')
            ->where('g.owner is null')
            ->getQuery()
            ->setMaxResults($limit)
            ->getResult();
    }
}