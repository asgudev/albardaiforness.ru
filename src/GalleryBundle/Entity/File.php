<?php

namespace GalleryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * File
 *
 * @ORM\Table(name="file")
 * @ORM\Entity(repositoryClass="GalleryBundle\Entity\FileRepository")
 * @ORM\HasLifecycleCallbacks
 */
class File
{
    const FILE_IMAGE = 0;
    const FILE_DOCUMENT = 1;
    const FILE_VIDEO = 2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\Column(type="string", name="file_path", length=255, nullable=true)
     */
    public $file;

    /**
     * @Assert\File(maxSize="60000000")
     */
    private $fileFile;

    /**
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="year", type="text", nullable=true)
     */
    private $year;

    /**
     * @ORM\ManyToOne(targetEntity="FamilyTreeBundle\Entity\Person", inversedBy="files")
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity="FamilyTreeBundle\Entity\PersonTag", mappedBy="file", cascade={"remove"})
     */
    private $personTags;

    /**
     * @ORM\Column(name="source", type="integer", nullable=true)
     */
    private $source;

    /**
     * @ORM\Column(name="is_background", type="boolean")
     */
    private $isBackground;

    /**
     * @return File
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->type = self::FILE_IMAGE;
        $this->isBackground = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return File
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return File
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return File
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set owner
     *
     * @param \FamilyTreeBundle\Entity\Person $owner
     *
     * @return File
     */
    public function setOwner(\FamilyTreeBundle\Entity\Person $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \FamilyTreeBundle\Entity\Person
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Add personTag
     *
     * @param \FamilyTreeBundle\Entity\PersonTag $personTag
     *
     * @return File
     */
    public function addPersonTag(\FamilyTreeBundle\Entity\PersonTag $personTag)
    {
        $this->personTags[] = $personTag;

        return $this;
    }

    /**
     * Remove personTag
     *
     * @param \FamilyTreeBundle\Entity\PersonTag $personTag
     */
    public function removePersonTag(\FamilyTreeBundle\Entity\PersonTag $personTag)
    {
        $this->personTags->removeElement($personTag);
    }

    /**
     * Get personTags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersonTags()
    {
        return $this->personTags;
    }




    

    private $temp;
    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFileFile(UploadedFile $file = null)
    {
        $this->fileFile = $file;
        if (isset($this->file)) {
            $this->temp = $this->file;
            $this->file = null;
        } else {
            $this->file = 'initial';
        }
        return $this;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFileFile()
    {
        return $this->fileFile;
    }

    public function getFileAbsolutePath()
    {
        return null === $this->file
            ? null
            : $this->getFileUploadRootDir().'/'.$this->file;
    }

    public function getFile()
    {
        return null === $this->file
            ? null
            : $this->getFileUploadDir().'/'.$this->file;
    }

    protected function getFileUploadRootDir()
    {
        return __DIR__ . '/../../../web/' .$this->getFileUploadDir();
    }

    protected function getFileUploadDir()
    {
        return 'uploads/files';
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preFileUpload()
    {
        if (null !== $this->getFileFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->file = $filename.'.'.$this->getFileFile()->guessExtension();
        }
        return $this;
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function uploadFile()
    {
        if (null === $this->getFileFile()) {
            return;
        }
        $this->getFileFile()->move($this->getFileUploadRootDir(), $this->file);

        // check if we have an old file
        if (isset($this->temp)) {
            // delete the old file
            unlink($this->getFileUploadRootDir().'/'.$this->temp);
            // clear the temp file path
            $this->temp = null;
        }
        $this->fileFile = null;
        return $this;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeFileUpload()
    {
        $file = $this->getFileAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }



    /**
     * Set source
     *
     * @param integer $source
     *
     * @return File
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return integer
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set isBackground
     *
     * @param boolean $isBackground
     *
     * @return File
     */
    public function setIsBackground($isBackground)
    {
        $this->isBackground = $isBackground;

        return $this;
    }

    /**
     * Get isBackground
     *
     * @return boolean
     */
    public function getIsBackground()
    {
        return $this->isBackground;
    }

    /**
     * Set year
     *
     * @param string $year
     *
     * @return File
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return string
     */
    public function getYear()
    {
        return $this->year;
    }
}
