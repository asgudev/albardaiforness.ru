<?php

namespace GalleryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FileUploadType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file_file', FileType::class, [
                'label' => 'File',
                'required' => true,
            ])
            ->add('title', TextType::class, [
                'label' => 'Title',
                'required' => false,
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description',
                'required' => false,
                'attr' => [
                    'rows' => 4
                ],
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'File type',
                'expanded' => true,
                'multiple' => false,
                'choices' => [
                    0 => 'Image',
                    1 => 'Document',
                    2 => 'Video',
                ],
                'placeholder' => false,
                'required' => false,
            ])

            ->add('submit', SubmitType::class, [
                'label' => 'Save',
                'attr' => [
                    'class' => 'btn-block',
                ]
            ]);

        if ($options["source"] == "gallery"){
            $builder
                ->add('isBackground', CheckboxType::class, [
                    'label' => 'Is background',
                    'required' => false,
                ])
                ->add('year', TextType::class, [
                    'label' => 'Year',
                    'required' => false,
                ])
            ;
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GalleryBundle\Entity\File',
            'source' => null,
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'GalleryBundle\Entity\File',
            'source' => null,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'file';
    }
}
