<?php
/**
 * Created by IntelliJ IDEA.
 * User: iProtoss
 * Date: 30.05.2016
 * Time: 12:32
 */

namespace FamilyTreeBundle\Command;


use FamilyTreeBundle\Entity\Dupe;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FindDupesCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('person:dupes')
            ->setDescription('Update files entities');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pr = $this->getDoctrine()->getRepository('TreeBundle:Person');
        $dr = $this->getDoctrine()->getRepository('TreeBundle:Dupe');
        $em = $this->getDoctrine()->getManager();
        try {

            for ($page = 0; $page < 99; $page++) {
                $dupes = $pr->findDupes($page);

                foreach ($dupes as $pair) {
                    $person1 = $pr->find($pr->find($pair['p1']));
                    $person2 = $pr->find($pr->find($pair['p2']));

                    $inDb = $dr->findDupe($person1, $person2);
                    if (!$inDb) {
                        $dupe = new Dupe();
                        $dupe->setPerson1($pr->find($pair['p1']));
                        $dupe->setPerson2($pr->find($pair['p2']));
                        $dupe->setStatus(true);
                        $em->persist($dupe);
                    }
                }
                $em->flush();
            }

        } catch (\Exception $ex) {

        }
    }

    private function getDoctrine()
    {
        return $this->getContainer()->get('doctrine');
    }
}