<?php
/**
 * Created by IntelliJ IDEA.
 * User: iProtoss
 * Date: 21.05.2016
 * Time: 20:15
 */

namespace FamilyTreeBundle\Command;


use FamilyTreeBundle\Entity\PersonTag;
use FamilyTreeBundle\Entity\Photo;
use GalleryBundle\Entity\File;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RebaseCommand extends ContainerAwareCommand
{
    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this
            ->setName('tree:rebase')
            ->setDescription('Update files entities');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $doctrine = $this->getContainer()->get('doctrine');

            $em = $doctrine->getManager();

            $photos = $doctrine->getRepository('TreeBundle:Photo')->findAll();

            /**
             * @var Photo $photo
             */
            foreach ($photos as $photo) {
                $file = new File();

                $file->setOwner($photo->getPerson());
                $file->setFile($photo->getFilePath());
                $file->setDescription($photo->getDescription());
                $em->persist($file);

                if ($photo->getMarkedPersons() != null) {
                    foreach ($photo->getMarkedPersonsArray() as $person_id => $tag) {
                        $person = $doctrine->getRepository('TreeBundle:Person')->find($person_id);
                        $personTag = new PersonTag($file, $person);

                        $sizes = getimagesize("web/".$file->getFile());
                        $coords = [
                            'naturalWidth' => $sizes[0]*1,
                            'naturalHeight' => $sizes[1]*1,
                            'x1' => $tag[0]*1,
                            'x2' => $tag[0]*1 + $tag[2]*1,
                            'y1' => $tag[1]*1,
                            'y2' => $tag[1]*1 + $tag[3]*1,
                        ];

                        $personTag->setMeta($coords);
                        $em->persist($personTag);
                    }
                }
                $em->remove($photo);
                $em->flush();


                /*
                                if (($photo->getMarkedPersons() != "") && ($photo->getMarkedPersons() != null)) {
                                    dump($photo);
                                    dump($photo->getMarkedPersonsArray());
                                    die();
                                }
                */
            }

            $output->writeln(sprintf('<info>Update demand status on non approved when the time has expired was successful done!</info>'));
        } catch (\Exception $e) {
            $output->writeln(sprintf('<error>Update demand status on non approved when the time has expired was unsuccessful done!</error>'));
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
        }
    }
}