<?php

namespace FamilyTreeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FamilyTreeBundle\Entity\Person;
use FamilyTreeBundle\Entity\User;
use FamilyTreeBundle\Form\PersonInfoType;

/**
 * User controller.
 *
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * Lists all Changes entities.
     *
     * @Route("/{id}", name="user")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $activity = $em->getRepository('TreeBundle:Log')->findBy(array("user" => $id), array('id' => 'DESC'));

        return array('activity' => $activity);
    }

}