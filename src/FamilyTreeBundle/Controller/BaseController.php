<?php

namespace FamilyTreeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{
    public function render($view, array $parameters = array(), Response $response = null)
    {
        $this->get('tree.visitor.service')->init();

        $visitorsData = $this->getDoctrine()->getRepository('TreeBundle:Visitor')->getVisitorData();

        $parameters['visitors'] = $visitorsData['visitorSum'];
        $parameters['pages'] = $visitorsData['pageSum'];
        $parameters['background'] = $this->getDoctrine()->getRepository('GalleryBundle:File')->findBackground();



        return parent::render($view, $parameters, $response);
    }

}
