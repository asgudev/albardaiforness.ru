<?php

namespace FamilyTreeBundle\Controller;

use FamilyTreeBundle\Entity\Marriage;
use FamilyTreeBundle\Entity\PersonTag;
use GalleryBundle\Entity\File;
use GalleryBundle\Form\FileEditType;
use GalleryBundle\Form\FileUploadType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FamilyTreeBundle\Entity\Person;
use FamilyTreeBundle\Entity\Log;
use FamilyTreeBundle\Form\PersonInfoType;
use FamilyTreeBundle\Controller\BaseController;

/**
 * Person controller.
 */
class PersonController extends BaseController
{
    /**
     * Displays a form to create a new Person entity.
     *
     * @Route("/admin/person/new", name="person_new")
     * @Method("GET")
     */
    public function newAction()
    {
        $entity = new Person();
        $form = $this->createForm(new PersonInfoType($this->get('security.context')), $entity, [
            'attr' => array('class' => 'editinfo'),
            'action' => $this->generateUrl('person_create'),
            'method' => 'POST',
        ]);

        return $this->render("@Tree/Person/new.html.twig", [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a Person entity.
     *
     * @Route("/person/{id}", name="person_show")
     * @Method("GET")
     */
    public function showAction(Request $request, Person $person)
    {

        if (!$person) {
            throw $this->createNotFoundException('Unable to find Person entity.');
        }

        $files = $this->getDoctrine()->getRepository("GalleryBundle:File")->findTagged($person);

        $timeline = [];


        return $this->render('@Tree/Person/show.html.twig', [
            'entity' => $person,
            'taggedFiles' => $files,
            'timeline' => $timeline,
        ]);
    }

    /**
     * Finds and displays a Family Tree.
     *
     * @Route("/person/{id}/tree", name="person_show_tree")
     * @Method("GET")
     */
    public function showFamilyTreeAction(Request $request, Person $person)
    {
        if (!$person) {
            throw $this->createNotFoundException('Unable to find Person entity.');
        }


        return $this->render("@Tree/Person/showFamilyTree.html.twig", [
            "id" => $person->getId(),
            "data" => $this->get('tree.tree.manager')->getFamilyTree($person)
        ]);
    }

    /**
     * Finds and displays a Person entity as JSON.
     *
     * @Route("/person/{id}/json", name="person_show_json")
     * @Method("GET")
     */
    public function showPersonJSONAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TreeBundle:Person')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Person entity.');
        }

        return new Response(json_encode($entity));
    }

    /**
     * Creates a new Person entity.
     *
     * @Route("/admin/person/", name="person_create")
     * @Method("POST")
     * @Template("TreeBundle:Person:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $person = new Person();

        $form = $this->createForm(new PersonInfoType($this->get('security.context')), $person, [
            'attr' => array('class' => 'editinfo'),
            'action' => $this->generateUrl('person_create'),
            'method' => 'POST',
        ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $person->setbirthYear('');
            $person->setdeathYear('');
            $person->setIsPublished(true);

            $em->persist($person);
            $em->flush();
            return $this->redirectToRoute('person_edit', ['id' => $person->getId()]);
        }

        return array(
            'form_info' => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Person entity.
     *
     * @Route("/admin/person/{id}/edit", name="person_edit")
     * @Method("GET")
     */
    public function editAction(Request $request, Person $person)
    {
        if (!$person) {
            throw $this->createNotFoundException('Unable to find Person entity.');
        }

        $editForm = $this->createForm(new PersonInfoType($this->get('security.context')), $person,
            [
                'attr' => array('class' => 'editinfo'),
                'action' => $this->generateUrl('person_update', array('id' => $person->getId())),
                'method' => 'POST',
            ]);

        $files = $this->getDoctrine()->getRepository("GalleryBundle:File")->findTagged($person);


        return $this->render("@Tree/Person/edit.html.twig", [
            'person' => $person,
            'edit_form' => $editForm->createView(),
            'taggedFiles' => $files,
        ]);
    }

    /**
     * Edits an existing Person entity.
     *
     * @Route("/admin/person/{id}/update", name="person_update")
     * @Method("POST")
     * @Template("TreeBundle:Person:edit.html.twig")
     */
    public function updateAction(Request $request, Person $entity)
    {

        $em = $this->getDoctrine()->getManager();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Person entity.');
        }

        $editForm = $this->createForm(new PersonInfoType($this->get('security.context')), $entity, array(
            'attr' => array('class' => 'editinfo'),
            'action' => $this->generateUrl('person_update', array('id' => $entity->getId())),
            'method' => 'POST',
        ));

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            if ($editForm->get('delete')->isClicked()) {
                $em->remove($entity);
                return $this->redirectToRoute('index');
            }

            $em->flush();
            return $this->redirect($this->generateUrl('person_edit', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }


    /**
     * @Route("/admin/person/edit/inline", name="person_edit_inline")
     * @Method("POST")
     */
    public function inlineEditAction(Request $request)
    {

        if ($this->get('security.context')->isGranted('ROLE_ADMIN')) {
            $em = $this->getDoctrine()->getManager();
            $request = $request->request->all();

            $id = $request["id"];
            $value = $request["value"];

            $searchCol = array(
                '1' => 'surname',
                '2' => 'name',
                '3' => 'nickname',
                '4' => 'birth',
                '5' => 'death'
            );

            if (!empty($request["columnId"])) {
                $col = $searchCol[$request["columnId"]];
            } else {
                $col = strtolower($request["columnName"]);
                $col = substr($col, strpos(trim($col), ' '));
            }

            $entity = $em->getRepository('TreeBundle:Person')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Person entity.');
            }

            switch ($col) {
                case "occupation" :
                    $entity->setOccupation($value);
                    break;
                case "note" :
                    $entity->setNote($value);
                    break;
                case "surname" :
                    $entity->setLastName($value);
                    break;
                case "name" :
                    $entity->setFirstName($value);
                    break;
                case "nickname" :
                    $entity->setNickname($value);
                    break;
                case "birth" :
                    $entity->setBirthAlt($value);
                    break;
                case "birthplace" :
                    $entity->setBirthPlace($value);
                    break;
                case "death" :
                    $entity->setDeathAlt($value);
                    break;
                case "deathplace" :
                    $entity->setDeathPlace($value);
                    break;
            }
            $em->flush();
            return new Response($request["value"]);
        }

        exit();
    }


    /**
     * @Route("/admin/person/{id}/edit/avatar/edit", name="edit_avatar_action")
     */
    public function editAvatarAction(Request $request, Person $person)
    {
        if ($request->isMethod("POST")) {
            $cropper = $this->get("gallery.cropper");

            if ($request->files->get("avatar_file")) {
                $file = new File();
                $file->setFileFile($request->files->get("avatar_file"))->preFileUpload()->uploadFile()->setOwner($person);
                $person->addFile($file);
                $person->setAvatarFile($file);

            }

            if ($request->get("avatar_data") != null) {
                $personTag = new PersonTag($file, $person);
                $data = json_decode($request->get("avatar_data"), true);
                $personTag->setMeta($data);

                $cropper->setSrc($file->getFile());
                $cropper->setDst("uploads/avatar");
                $cropper->setData($request->get("avatar_data"));
                $avatar = $cropper->crop();

                $person->setAvatarUrl("/" . $avatar);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($person);
            $em->persist($file);
            $em->persist($personTag);
            $em->flush();

            return new JsonResponse([
                'status' => true,
                'data' => [
                    'url' => $avatar
                ],
                "command" => "closePopup(this.popup);"
            ]);

        }

        $tag = $this->getDoctrine()->getRepository("TreeBundle:PersonTag")->findAvatarData($person);


        return $this->render("@Tree/Person/editAvatar.html.twig", [
            'person' => $person,
            'tag' => $tag,
        ]);
    }

    /**
     * @Route("/admin/person/{person}/remove/{type}/{relative}", name="person_remove_relative")
     */
    public function removeRelativeAction(Request $request, Person $person, $type = null, Person $relative)
    {
        $personManager = $this->get("tree.person.manager");
        switch ($type) {
            case "parent":
                $personManager->removeParent($person, $relative);
                return new JsonResponse([
                    'success' => true,
                    'command' => "this.link.parents('.person').remove();reloadBlock('#relatives', centerImages);",
                ]);
                break;
            case "spouse":
                $personManager->removeSpouse($person, $relative);
                return new JsonResponse([
                    'success' => true,
                    'command' => "this.link.parents('.person').remove();reloadBlock('#relatives', centerImages);",
                ]);
                break;
            case "child":
                $personManager->removeChild($person, $relative);
                return new JsonResponse([
                    'success' => true,
                    'command' => "this.link.parents('.person').remove();reloadBlock('#relatives', centerImages);",
                ]);
                break;
            case "brother":
                $personManager->removeBrother($person, $relative);
                return new JsonResponse([
                    'success' => true,
                    'command' => "this.link.parents('.person').remove();reloadBlock('#relatives', centerImages);",
                ]);
                break;
        }

        return new JsonResponse([
            'succes' => false,
        ]);
    }

    /**
     * @Route("/admin/person/{person}/add/{type}/{relative}", name="person_add_relative")
     */
    public function addRelativeAction(Request $request, Person $person, $type = null, Person $relative)
    {
        $personManager = $this->get("tree.person.manager");
        switch ($type) {
            case "parent":
                $personManager->addParent($person, $relative);
                return new JsonResponse([
                    'success' => true,
                    'command' => "closePopup(this.popup); reloadBlock('#relatives', centerImages);",
                ]);
                break;
            case "spouse":
                $personManager->addSpouse($person, $relative);
                return new JsonResponse([
                    'success' => true,
                    'command' => "closePopup(this.popup); reloadBlock('#relatives', centerImages);",
                ]);
                break;
            case "child":
                $personManager->addChild($person, $relative);
                return new JsonResponse([
                    'success' => true,
                    'command' => "closePopup(this.popup); reloadBlock('#relatives', centerImages);",
                ]);
                break;
            case "brother":
                $personManager->addBrother($person, $relative);
                return new JsonResponse([
                    'success' => true,
                    'command' => "closePopup(this.popup); reloadBlock('#relatives', centerImages);",
                ]);
                break;
        }

        return new JsonResponse([
            'succes' => false,
        ]);
    }

    /**
     * @Route("/admin/person/{id}/add/{type}", name="person_add_relative_form")
     */
    public function addParentAction(Request $request, Person $person, $type)
    {
        //TODO: implement suggestions

        switch ($type) {
            case "parent":
                $title = "Add parent";
                break;
            case "spouse":
                $title = "Add spouse";
                break;
            case "child":
                $title = "Add child";
                break;
            case "brother":
                $title = "Add brother";
                break;
        }

        return $this->render('@Tree/Person/addRelative.html.twig', [
            'title' => $title,
            'suggestions' => [],
            'baseUrl' => '/admin/person/' . $person->getId() . '/add/' . $type . '/__relative__',
        ]);
    }
}
