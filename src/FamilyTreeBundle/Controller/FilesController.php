<?php

namespace FamilyTreeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use FamilyTreeBundle\Entity\Person;

use GalleryBundle\Entity\File;
use GalleryBundle\Form\FileEditType;
use GalleryBundle\Form\FileUploadType;


class FilesController extends Controller
{
    /**
     * @Route("/admin/file/{id}/edit", name="person_edit_file")
     */
    public function fileEditAction(Request $request, File $file)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(FileEditType::class, $file, [
            'action' => $this->generateUrl('person_edit_file', [
                'id' => $file->getId()
            ]),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {

                if ($form->get('delete')->isClicked()) {

                    $data = [
                        'status' => true,
                        'command' => 'closePopup(this.popup);$(".file_' . $file->getId() . '").remove();',
                    ];

                    $em->remove($file);
                    $em->flush();

                    return new JsonResponse($data);
                } elseif ($form->get('submit')->isClicked()) {
                    $em->persist($file);
                    $em->flush();
                    $data = [
                        'status' => true,
                        'file' => [
                            'path' => $file->getFile(),
                            'title' => $file->getTitle(),
                            'type' => $file->getType(),
                        ],
                        'command' => "location.reload();",
                    ];
                    return new JsonResponse($data);
                }
            } else {
                $data = [
                    'status' => false,
                    'error' => $form->getErrors()
                ];
                return new JsonResponse($data);
            }
        }


        return $this->render("@Gallery/Files/editFile.html.twig", [
            'form' => $form->createView(),
            'file' => $file
        ]);
    }

    /**
     * @Route("/admin/person/{id}/upload", name="person_upload_file")
     */
    public function fileUploadAction(Request $request, Person $person)
    {
        if (!$person) {
            throw $this->createNotFoundException('Unable to find Person entity.');
        }
        $file = new File();
        $file->setOwner($person);
        $form = $this->createForm(FileUploadType::class, $file, [
            'action' => $this->generateUrl('person_upload_file', [
                'id' => $person->getId()
            ])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($file);
                $em->flush();

                $editform = $this->createForm(FileEditType::class, $file, [
                    'action' => $this->generateUrl('person_edit_file', [
                        'id' => $file->getId()
                    ]),
                ]);

                $editpage = $this->renderView("@Gallery/Files/editFile.html.twig", [
                    'form' => $editform->createView(),
                    'file' => $file
                ]);

                $data = [
                    'status' => true,
                    'file' => [
                        'path' => $file->getFile(),
                        'title' => $file->getTitle(),
                        'type' => $file->getType(),
                    ],
                    'form' => $editpage,
                    'command' => "this.popup.container.html(response.form);",

                ];

                return new JsonResponse($data);
            } else {
                $data = [
                    'status' => false,
                    'error' => $form->getErrors()
                ];
                return new JsonResponse($data);
            }
        }


        return $this->render("@Gallery/Files/uploadFile.html.twig", [
            'form' => $form->createView()
        ]);
    }


}