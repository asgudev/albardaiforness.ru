<?php

namespace FamilyTreeBundle\Controller;

use Elastica\Query\QueryString;
use FamilyTreeBundle\Entity\Person;
use FOS\ElasticaBundle\HybridResult;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FamilyTreeBundle\Form\MainSearchType;
use FamilyTreeBundle\Form\ShortSearchType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Symfony\Component\HttpFoundation\JsonResponse;

class SearchController extends BaseController
{
    /**
     * @Route("/search/", name="search_person")
     */
    public function searchResultAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $shortform = $this->createForm(new ShortSearchType(), null, array(
            'action' => $this->generateUrl('search_person'),
            'method' => 'GET',
        ));
        $shortform->handleRequest($request);
        $form = $this->createForm(new MainSearchType(), null, array(
            'action' => $this->generateUrl('search_person'),
            'method' => 'POST',
        ));
        $form->handleRequest($request);

        $combo = $shortform->getViewData();

        if ($combo["q"] != null) {
            $persons = $this->getDoctrine()->getRepository("TreeBundle:Person")->findByQuery($combo["q"]);

            return $this->render("@Tree/Default/searchResult.html.twig", [
                'form' => $form->createView(),
                'short_form' => $shortform->createView(),
                'persons' => $persons,
            ]);
        }

        $request = $request->request->all();

        if (strtolower($request["note"]) == "admin") {
            return $this->redirect($this->generateUrl('fos_user_security_login'));
        };
        if (!empty($request)) {
            $relative_form = array(
                'parentName',
                'childName',
                'couseName',
                'broName'
            );
            $persons = $em->createQueryBuilder('p')->select('p')->from('TreeBundle:Person', 'p')->where('p.id > 0');
            $check_relatives = false;

            foreach ($relative_form as $form_check_rel) {
                if ($request[$form_check_rel] != "") {
                    $check_relatives = true;
                    break;
                }
            }

            if ($check_relatives) {
                $relatives = $em->createQueryBuilder('r')->select('r')->from('TreeBundle:Person', 'r')->where('r.id > 0');
            }

            $i = 0;
            foreach ($request as $key => $val) {
                if (($request[$key] != '') && ($key != '_token')) {
                    if (in_array($key, array('birthYear', 'deathYear'))) {
                        if (preg_match("/\d{4}-\d{4}/", $val)) {
                            preg_match_all("/(\d{4})-(\d{4})/", $val, $dates);
                            $persons = $persons->andWhere('p.' . $key . ' BETWEEN ?' . $i . ' AND ?' . ($i + 1))->setParameter($i, $dates[1][0])->setParameter($i + 1, $dates[2][0]);
                            $i++;
                            $i++;
                        } elseif (in_array(substr($val, 0, 1), array('<', '>'))) {
                            $year = substr($val, 1, strlen($val));
                            $persons = $persons->andWhere('p.' . $key . ' ' . substr($val, 0, 1) . ' ?' . $i)->setParameter($i, $year);
                            $i++;
                        } else {
                            $persons = $persons->andWhere('p.' . $key . ' = ?' . $i)->setParameter($i, $val);
                            // dump($persons); die();
                            $i++;
                        }
                    } elseif (in_array($key, $relative_form)) {
                        $relatives = $relatives->andWhere('r.firstName LIKE :q or r.lastName LIKE :q or r.nickname LIKE :q')->setParameter('q', '%' . $val . '%');
                        $search_relatives[] = $key;
                    } elseif ($key == "id") {
                        $persons = $persons->andWhere('p.id = ?' . $i)->setParameter($i, $val);
                        $i++;
                    } elseif ($key == "firstName") {
                        $persons = $persons->andWhere('p.firstName LIKE ?' . $i . ' or p.firstName = ?' . ($i + 1))->setParameter($i, '%' . $val . "%")->setParameter($i + 1, $val);
                        $i++;
                        $i++;
                    } elseif ($key == "anything") {
                        $anyval = str_replace(" ", "%", $val);
                        $concatFields = array(
                            'p.id',
                            'p.firstName',
                            'p.lastName',
                            'p.lastName',
                            'p.firstName',
                            'p.birthYear',
                            'p.deathYear',
                        );
                        foreach ($concatFields as $field) {
                            if (!isset($searchIn)) {
                                $searchIn = $persons->expr()->concat($persons->expr()->literal(''), $field);
                                continue;
                            }

                            $searchIn = $persons->expr()->concat($searchIn, $persons->expr()->concat($persons->expr()->literal(''), $field));
                        }

                        $persons->add('where', $persons->expr()->like($searchIn, ':keyword'));
                        $persons->setParameter('keyword', '%' . strtolower($anyval) . '%');
                    } else {
                        $persons = $persons->andWhere('p.' . $key . ' LIKE ?' . $i)->setParameter($i, '%' . $val . '%');
                        $i++;
                    }
                }
            }

            if ($check_relatives) {
                $relatives = $relatives->getQuery();
                $relatives = $relatives->getResult();
            }

            $persons = $persons->getQuery();
            //dump($persons); die();
            $persons = $persons->getResult();
            // dump($persons); die();
            if ($check_relatives) {
                foreach ($persons as $person) {
                    foreach ($search_relatives as $search_rel_param) {
                        switch ($search_rel_param) {
                            case "childName":
                                $rels_for_check = $person->getChildren();
                                break;

                            case "parentName":
                                $rels_for_check = $person->getParents();
                                break;

                            case "couseName":
                                $rels_for_check = $person->getMarriages();
                                break;

                            case "broName":
                                $rels_for_check = $rels_for_check = $person->getBrotherhood();
                                break;
                        }

                        foreach ($rels_for_check as $rel) {
                            if (in_array($rel, $relatives)) $person_final[] = $person;
                        }
                    }
                }
            } else {
                return $this->render("@Tree/Default/searchResult.html.twig", [
                    'form' => $form->createView(),
                    'short_form' => $shortform->createView(),
                    'persons' => $persons,
                ]);
            }
        } else {
            $persons = array();
        }

        //$searchd = $this->get('iakumai.sphinxsearch.search');
        //dump($searchd); die();

        return $this->render("@Tree/Default/searchResult.html.twig", [
            'form' => $form->createView(),
            'short_form' => $shortform->createView(),
            'persons' => $person_final,
        ]);
    }

    /**
     * @Route("/search_person_by_id/", name="search_person_by_id")
     */
    public function searchByIdAction(Request $request)
    {
        //print_r($request->get('query', false));die();
        $em = $this->getDoctrine()->getManager();
        $response = [];
        if ($request->get('query', false)) {
            //$id_list = explode(",", str_replace("@", "", $request->get('q')));

            $persons = $em
                ->createQueryBuilder('p')
                ->select('p')->from('TreeBundle:Person', 'p')->where('p.id IN (:ids)')->setParameter('ids', explode(',', $request->get('query')))->getQuery()->getResult();

            /**
             * @var Person $person
             */
            foreach ($persons as $person) {
                $photo = $person->getAvatar();
                $response['suggestions'][] = array(
                    'data' => $person->getId(),
                    'id' => $person->getId(),
                    'firstName' => $person->getFirstName(),
                    'lastName' => $person->getLastName(),
                    'nickname' => $person->getNickname(),
                    'dates' => $person->getDates(),
                    'birth' => is_object($person->getBirth()) ? $person->getBirth()->format('d/m/Y') : '',
                    'death' => is_object($person->getDeath()) ? $person->getDeath()->format('d/m/Y') : '',
                    'photo' => $person->getAvatar(),
                    'names' => $person->getNames(),
                );
            }

            // print_r(count($person->getPhotos()));die();

            $response['total_count'] = count($persons);
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/search_ajax", name="search_person_ajax")
     */
    public function searchByQueryAction(Request $request)
    {
        $response = [];
        if ($request->get('query') != null) {
            $query = $request->get('query');

            $persons = $this->getDoctrine()->getRepository("TreeBundle:Person")->findByQuery($query);

            /**
             * @var Person $person
             */
            foreach ($persons as $person) {
                $response['suggestions'][] = array(
                    'id' => $person->getId(),
                    'firstname' => $person->getFirstName(),
                    'lastname' => $person->getLastName(),
                    'nickname' => $person->getNickname(),
                    'names' => $person->getNames(),
                    'dates' => $person->getDates(),
                    'photo' => $person->getAvatarUrl(),
                );
            }

            return new JsonResponse($response);

        }
        return new JsonResponse($response);

    }
}