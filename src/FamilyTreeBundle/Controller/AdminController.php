<?php

namespace FamilyTreeBundle\Controller;

use FamilyTreeBundle\Entity\Message;
use FamilyTreeBundle\Entity\Person;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use FamilyTreeBundle\Entity\Invitation;


class AdminController extends Controller
{
    /**
     * @Route("/admin/managers/", name="managers")
     * @Method("GET")
     * @Template()
     */
    public function managersAction(Request $request)
    {
        $managers = $this->getDoctrine()->getRepository("UserBundle:User")->findAll();
        $page = $request->get("page") !== null ? $request->get("page") : 0;
        $activity = $this->getDoctrine()->getRepository("TreeBundle:Log")->findByPage($page);

        return array('managers' => $managers, 'activity' => $activity);
    }

    /**
     * @Route("/admin/managers/set_region", name="manager_set_region")
     * @Method("POST")
     */
    public function managerSetRegionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->find('TreeBundle:User', $request->get('user'));
        $user->setRegion($request->get('region'));
        $em->flush();

        return new Response(time());
    }

    /**
     * @Route("/admin/lock/", name="lock_user")
     * @Method("POST")
     */
    public function lockAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->find('TreeBundle:User', $request->get('id'));
        //$user->setLocked($request->get('locked'));

        $em->remove($user);
        $em->flush();

        return new Response();
    }

    /**
     * @Route("/admin/invitation/new/", name="new_invitation")
     * @Method("POST")
     */
    public function newInvitationAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $invitation = new Invitation();
        $invitation->setEmail($request->get('email'));
        $invitation->send();
        $em->persist($invitation);
        $em->flush();

        $message = \Swift_Message::newInstance()->
        setSubject('Invitation to Albardaifornes.ru')->
        setFrom($this->container->getParameter('mailer_user'))->
        setTo($request->get('email'))->
        setBody('De Santa Bruno invites you to be manager of Albardaiforess.ru<br />' . 'Please follow the <a href="http://albardaiforness.ru/register/">http://albardaiforness.ru/register/</a> and enter this code there: <b>' . $invitation->getCode() . "</b>", 'text/html');
        $this->get('mailer')->send($message);

        return $this->redirect($this->generateUrl('managers'));
    }

    /**
     * @Route("/admin/messages/delete/{id}/", name="delete_message")
     * @Method("POST")
     */
    public function deleteMessageAction(Request $request, Message $message)
    {
        if (!$message) {
            throw $this->createNotFoundException('Unable to find Message entity.');
        }
        $em = $this->getDoctrine()->getManager();

        $em->remove($message);
        $em->flush();
        return $this->redirect($this->generateUrl('messages'));
    }


    /**
     * @Route("/admin/translation/apply", name="translation_apply")
     * @Method("POST")
     */
    public function applyTranslationAction()
    {
        exec('php ../app/console cache:clear -e=prod', $out);
        return new Response('updated');
    }

    /**
     * Lists all Messages entities
     *
     * @Route("/admin/messages/", name="messages")
     * @Method("GET")
     */
    public function listMessagesAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('TreeBundle:Message')->findAll();

        return $this->render("@Tree/Admin/messages.html.twig", array(
            'entities' => $entities,
        ));
    }

    /**
     * @Route("/admin/duplicates", name="admin_duplicates_search")
     */
    public function searchDuplicatesAction(Request $request)
    {
        $page = $request->get('page');
        //$dupes = $this->getDoctrine()->getRepository("TreeBundle:Person")->findDupes($page);
        $dupes = $this->getDoctrine()->getRepository("TreeBundle:Dupe")->findAll();
        /*$persons_ids = [];

        foreach ($dupes as $dupe) {
            if (!in_array($dupe["p1"], $persons_ids)) $persons_ids[] = $dupe["p1"];
            if (!in_array($dupe["p2"], $persons_ids)) $persons_ids[] = $dupe["p2"];
        }
        $persons = $this->getDoctrine()->getRepository("TreeBundle:Person")->findByIds($persons_ids);

        $persons_array = [];
        foreach ($persons as $person) {
            $persons_array[$person->getId()] = $person;
        }

        $person_dupes = [];
        foreach ($dupes as $dupe) {
            $person_dupes[] = [
                $dupe["p1"] => $persons_array[$dupe["p1"]],
                $dupe["p2"] => $persons_array[$dupe["p2"]],
            ];
        }

        if ($request->get("ajax") !== null) {
            $twig = $this->get('twig');
            $template = $twig->loadTemplate("@Tree/Admin/dupesList.html.twig");

            return new Response($template->renderBlock("content",[
                'dupes' => $person_dupes
            ]));
        } else {
            return $this->render('@Tree/Admin/dupesList.html.twig', [
                'dupes' => $person_dupes
            ]);
        }*/
        return $this->render('@Tree/Admin/dupesList.html.twig', [
            'dupes' => $dupes
        ]);
    }

    /**
     * @Route("/admin/compare/{person1}/{person2}", name="compare_person")
     */
    public function comparePerson(Request $request, Person $person1, Person $person2)
    {

        return $this->render("@Tree/Admin/compare.html.twig", [
            'persons' => [
                $person1, $person2
            ]
        ]);
    }

    /**
     * @Route("/admin/notdupe/{person1}/{person2}", name="not_a_dupe_person")
     */
    public function notDupePersonAction(Request $request, Person $person1, Person $person2)
    {
        $em = $this->getDoctrine()->getManager();

        $dupe = $this->getDoctrine()->getRepository("TreeBundle:Dupe")->findDupe($person1, $person2);
        $dupe->setStatus(false);

        $dupeInv = $this->getDoctrine()->getRepository("TreeBundle:Dupe")->findDupe($person2, $person1);
        $dupeInv->setStatus(false);

        $em->flush();

        return $this->redirectToRoute('admin_duplicates_search');
    }


}