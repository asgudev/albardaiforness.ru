<?php

namespace FamilyTreeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FamilyTreeBundle\Form\MainSearchType;
use FamilyTreeBundle\Form\PersonType;
use FamilyTreeBundle\Form\PersonInfoType;
use FamilyTreeBundle\Entity\Person;
use FamilyTreeBundle\Entity\Changes;
use FamilyTreeBundle\Entity\Invitation;
use FamilyTreeBundle\Entity\FieldChange;

/**
 * Gallery controller.
 */
class GalleryController extends Controller
{

}
