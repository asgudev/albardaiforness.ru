<?php

namespace FamilyTreeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FamilyTreeBundle\Entity\Changes;
use FamilyTreeBundle\Entity\Person;
use FamilyTreeBundle\Entity\User;
use FamilyTreeBundle\Entity\Photo;
use FamilyTreeBundle\Form\ChangesType;
use FamilyTreeBundle\Form\PersonInfoType;

/**
 * Changes controller.
 *
 * @Route("/admin/changes")
 */
class ChangesController extends Controller
{

    /**
     * Lists all Changes entities.
     *
     * @Route("/", name="changes")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $region = $this->get('security.context')->getToken()->getUser()->getRegion();

        if ($region == "all")
        {
            $entities = $em->getRepository('TreeBundle:Changes')->findAll();
        }
        else
        {
            $entities = $em->getRepository('TreeBundle:Changes')->findBy( array( "region" => $region ) );
        }
		
		foreach($entities as $change)
		{
			$spouses = $change->getPerson()->getspousesString();
			$children = $change->getPerson()->getchildrenString();
			$bro = $change->getPerson()->getbrotherhoodString(); 
		}
		
        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Changes entity.
     *
     * @Route("/", name="changes_create")
     * @Method("POST")
     * @Template("TreeBundle:Changes:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Changes();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('changes_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Changes entity.
     *
     * @param Changes $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Changes $entity)
    {
        $form = $this->createForm(new ChangesType(), $entity, array(
            'action' => $this->generateUrl('changes_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Changes entity.
     *
     * @Route("/new", name="changes_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Changes();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Changes entity.
     *
     * @Route("/{id}", name="changes_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TreeBundle:Changes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Changes entity.');
        }
		
		$person = $entity->getPerson();

        $deleteForm = $this->createDeleteForm($id);
		$form = $this->createPersonEditForm($person->getId());
		
		$changes = array();
		foreach ($entity->getChanges() as $field)
		{
			$changes[$field->getField()] = $field->getValue();
		}
		
		$changes['parents_remove'] = array_diff( explode(',', $person->getparentsString()), explode(',', $changes['parentsString']));
		$changes['couse_remove'] = array_diff( explode(',', $person->getspousesString()), explode(',', $changes['spousesString']));
		
		$changes['child_remove'] = array_diff( explode(',', $person->getchildrenString()), explode(',', $changes['childrenString']));
		$changes['bro_remove'] = array_diff( explode(',', $person->getbrotherhoodString()), explode(',', $changes['brotherhoodString']));

        return array(
            'entity'      => $entity,
            'person'      => $person,
            'changes'     => $changes,
            'delete_form' => $deleteForm->createView(),
            'form' 	      => $form->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Changes entity.
     *
     * @Route("/{id}/edit", name="changes_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TreeBundle:Changes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Changes entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Changes entity.
    *
    * @param Changes $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Changes $entity)
    {
        $form = $this->createForm(new ChangesType(), $entity, array(
            'action' => $this->generateUrl('changes_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
	
    /**
     * Edits an existing Changes entity.
     *
     * @Route("/{id}", name="changes_update")
     * @Method("PUT")
     * @Template("TreeBundle:Changes:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TreeBundle:Changes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Changes entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('changes_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
	
    /**
     * Deletes a Changes entity.
     *
     * @Route("/{id}", name="changes_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('TreeBundle:Changes')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Changes entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('changes'));
    }

    /**
     * Creates a form to delete a Changes entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('changes_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
	
	/**
     * Creates a form to edit Person Changes entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createPersonEditForm($id)
    {
		$em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('TreeBundle:Person')->find($id);
		
		$form = $this->createForm(new PersonInfoType($this->get('security.context')), $entity, array(
            'attr' => array('class'=>'editinfo'),
            'action' => $this->generateUrl('person_update', array('id' => $entity->getId())),
            'method' => 'POST',
            'em' => $this->getDoctrine()->getManager()
        ));

        $form->add('submit', 'submit', array('label' => 'Save', 'attr'=>array('class'=>'grbutton')));

        return $form;
    }
	
	/**
	 * @Route("/accept/{id}/", name="accept")
	 * @Method("POST")
	 * @Template()
	 * @ParamConverter("changes", class="TreeBundle:Changes")
	 */
	public function acceptAction(Request $request, $changes)
	{
		
		$person = $changes->getPerson();
        
		foreach($changes->getChanges() as $change)
		{
			
			if ($change->getField() == "photo")
			{
				$prior = 0;
				foreach ($person->getPhotos() as $photo) {
					if ($photo->getPrior() > $prior) {
						$prior = $photo->getPrior();
					}
				}
				$prior++;
				
				$name = $change->getValue("url");
				$newname = $person->getId().'-'.$prior.'.jpg';
				
				$path_name = "uploads/suggests/".$name;
				$path_new_name = "uploads/photos/".$newname;
				
				rename($path_name, $path_new_name); 
				
				$photo = new Photo();
				
				$photo->setFilePath($newname)
				->setPrior($prior)
				->setPerson($person);
				
				$person->addPhoto($photo);
			} elseif ($change->getField() == "avatar") {
				
				$prior = 0;
				foreach ($person->getPhotos() as $photo) {
					if ($photo->getPrior() > $prior) {
						$prior = $photo->getPrior();
					}
				}
				$prior++;
				
				$name = $change->getValue("url");
				$newname = $person->getId().'-'.$prior.'.jpg';
				
				$path_name = "uploads/suggests/".$name;
				$path_new_name = "uploads/photos/".$newname;
				
				rename($path_name, $path_new_name); 
				
				$photo = new Photo();
				
				$photo->setFilePath($newname)
				->setPrior($prior)
				->setPerson($person);
				
				$person->addPhoto($photo);
				
				
				//dump($photo); die();
				
				
				if (substr($person->getAvatar(), -5, 5) != "e.png")
				{	
					$prior++;
					
					$aname = $person->getAvatar();
					$anewname = $person->getId().'-'.$prior.'.jpg';
					
					if (substr($aname, 0, 1) == "/") $aname = substr($aname, 1);
					$apath_name = $aname;
					$apath_new_name = "uploads/photos/".$anewname;

					rename($apath_name, $apath_new_name); 
					
					$prev_avatar = new Photo();
					$prev_avatar->setFilePath($anewname)
					->setPrior($prior)
					->setPerson($person);
					
					$person->addPhoto($prev_avatar);
				}
				$person->setAvatar("/".$path_new_name);
				
				$person->addPhoto($photo);
				//dump($person); die();
				
			} else {
				$set = 'set' . ucfirst($change->getField());
				$get = 'get' . ucfirst($change->getField());
				
				if (is_object($person->$get()))
				{
					$person->$set(new \DateTime($change->getValue()));
				}
				else
				{
					if (($set == "setDeath") || ($set == "setBirth")){
						$person->$set(new \DateTime($change->getValue()));
					} else {
						$person->$set($change->getValue());
					}
				}
			}
		}
        
		$this->getDoctrine()->getManager()->flush($person);
		
		
		$em = $this->getDoctrine()->getManager();
		$em->remove($changes);
		$em->flush();
		
		return $this->redirect($this->generateUrl('changes'));
	}

	/**
	 * @Route("/decline/{id}/", name="decline")
	 * @Method("POST")
	 * @Template()
	 * @ParamConverter("changes", class="TreeBundle:Changes")
	 */
	public function declineAction($id, Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$entity = $em->getRepository('TreeBundle:Changes')->find($id);
		if (!$entity)
		{
			throw $this->createNotFoundException('Unable to find Changes entity.');
		}

		$em->remove($entity);
		$em->flush();
		return $this->redirect($this->generateUrl('changes'));
	}
	
}
