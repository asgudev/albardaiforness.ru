<?php
namespace FamilyTreeBundle\Controller;

use FOS\ElasticaBundle\HybridResult;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FamilyTreeBundle\Form\MainSearchType;
use FamilyTreeBundle\Form\ShortSearchType;
use FamilyTreeBundle\Form\PersonInfoType;
use FamilyTreeBundle\Form\MessageType;
use FamilyTreeBundle\Entity\Person;
use FamilyTreeBundle\Entity\Visitor;
use FamilyTreeBundle\Entity\Changes;
use FamilyTreeBundle\Entity\Message;
use FamilyTreeBundle\Entity\Invitation;
use FamilyTreeBundle\Entity\FieldChange;

class DefaultController extends BaseController
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {

        //$request->getSession()->invalidate();die();

        $form = $this->createForm(new MainSearchType(), null, array(
            'action' => $this->generateUrl('search_person'),
            'method' => 'POST',
        ));

        $short_form = $this->createForm(new ShortSearchType(), null, array(
            'action' => $this->generateUrl('search_person'),
            'method' => 'GET',
        ));

        return $this->render('@Tree/Default/index.html.twig',
            [
                'form' => $form->createView(),
                'short_form' => $short_form->createView()
            ]
        );
    }

    /**
     * @Route("/suggest/{id}", name="suggest_changes")
     * @Method("GET")
     * @Template()
     */
    public function suggestAction(Request $request, Person $person)
    {
        $form = $this->createForm(new PersonInfoType($this->get('security.context')), $person, array(
            'attr' => array(
                'class' => 'editinfo'
            ),
            'action' => $this->generateUrl('create_changes', array(
                'id' => $person->getId()
            )),
            'method' => 'PUT',
            'em' => $this->getDoctrine()->getManager()
        ));
        $form->add('author', 'text', array(
            'mapped' => false
        ));
        $form->add('region', 'choice', array(
            'choices' => array('IT' => 'Italy', 'CE' => 'Central Europe', 'NA' => 'North America', 'SA' => 'South America', 'AU' => 'Australia',),
            'required' => true, 'mapped' => false));

        $form->add('comment', 'textarea', array(
            'mapped' => false
        ));
        $form->add('submit', 'submit', array(
            'label' => 'Suggest'
        ));
        return array(
            'entity' => $person,
            'form' => $form->createView()
        );
    }

    /**
     * Edits an existing Person entity.
     *
     * @Route("/suggest/{id}/", name="create_changes")
     * @Method("PUT")
     * @Template("TreeBundle:Default:suggest.html.twig")
     */
    public function createChangesAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->find('TreeBundle:Person', $id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Person entity.');
        }

        $editForm = $this->createForm(new PersonInfoType($this->get('security.context')), $entity, array(
            'attr' => array('class' => 'editinfo'),
            'action' => $this->generateUrl('create_changes', array('id' => $entity->getId())),
            'method' => 'PUT',
            'em' => $this->getDoctrine()->getManager()
        ));

        $editForm->add('author', 'text', array('mapped' => false));
        $editForm->add('region', 'text', array('mapped' => false));
        $editForm->add('comment', 'textarea', array('mapped' => false));
        $editForm->add('submit', 'submit', array('label' => 'Suggest'));
        $editForm->handleRequest($request);
        if ($editForm->isValid()) {

            $person = $request->get('sandello_familytreebundle_person');

            if (isset($person['age']) && $person['age'] != '' && $entity->getBirth() == null && is_object($entity->getDeath())) {
                $birthDate = clone $entity->getDeath();
                $entity->setBirth($birthDate->sub(new DateInterval('P' . $person['age'] . 'Y')));
            }

            $uow = $em->getUnitOfWork();
            $uow->computeChangeSets();

            $changeset = $uow->getEntityChangeSet($entity);


            $changes = new Changes($person['author'], $person['comment'], $person['region'], new \DateTime('NOW'), $entity);


            foreach ($changeset as $field => $value) {
                if ($field == "privateNote") continue;
                if ($value[0] != $value[1]) {
                    if (is_object($value[1])) {
                        $value[1] = $value[1]->format('Y-m-d');
                    }

                    $change = new FieldChange($field, $value[1], $changes);
                    $em->persist($change);
                }
            }


            if ($person['parentsString'] != $entity->getparentsString()) {

                $parents = explode(',', $person['parentsString']);
                $current = explode(',', $entity->getparentsString());

                $delete = array_diff($current, $parents);
                $add = array_diff($parents, $current);
                //dump($add);

                if (isset($add)) {
                    foreach ($add as $new) {
                        if ($new != "") {
                            $change = new FieldChange('parentsString', "add " . $new, $changes);
                            $em->persist($change);
                        }
                    }
                }
                if (isset($delete)) {
                    foreach ($delete as $del) {
                        if ($del != "") {
                            $change = new FieldChange('parentsString', "delete " . $del, $changes);
                            $em->persist($change);
                        }
                    }
                }


            }
            if ($person['spousesString'] != $entity->getspousesString()) {
                $spouses = explode(',', $person['spousesString']);
                $current = explode(',', $entity->getspousesString());

                $delete = array_diff($current, $spouses);
                $add = array_diff($spouses, $current);


                if (isset($add)) {
                    foreach ($add as $new) {
                        if ($new != "") {
                            $change = new FieldChange('spousesString', "add " . $new, $changes);
                            $em->persist($change);
                        }
                    }
                }
                if (isset($delete)) {
                    foreach ($delete as $del) {
                        if ($del != "") {
                            $change = new FieldChange('spousesString', "delete " . $del, $changes);
                            $em->persist($change);
                        }
                    }
                }

            }
            if ($person['childrenString'] != $entity->getchildrenString()) {
                $child = explode(',', $person['childrenString']);
                $current = explode(',', $entity->getchildrenString());

                $delete = array_diff($current, $child);
                $add = array_diff($child, $current);


                if (isset($add)) {
                    foreach ($add as $new) {
                        if ($new != "") {
                            $change = new FieldChange('childrenString', "add " . $new, $changes);
                            $em->persist($change);
                        }
                    }
                }
                if (isset($delete)) {
                    foreach ($delete as $del) {
                        if ($del != "") {
                            $change = new FieldChange('childrenString', "delete " . $del, $changes);
                            $em->persist($change);
                        }
                    }
                }

            }
            if ($person['brotherhoodString'] != $entity->getbrotherhoodString()) {
                $bro = explode(',', $person['brotherhoodString']);
                $current = explode(',', $entity->getbrotherhoodString());

                $delete = array_diff($current, $bro);
                $add = array_diff($bro, $current);


                if (isset($add)) {
                    foreach ($add as $new) {
                        if ($new != "") {
                            $change = new FieldChange('brotherhoodString', "add " . $new, $changes);
                            $em->persist($change);
                        }
                    }
                }
                if (isset($delete)) {
                    foreach ($delete as $del) {
                        if ($del != "") {
                            $change = new FieldChange('brotherhoodString', "delete @" . $del, $changes);
                            $em->persist($change);
                        }
                    }
                }
                //dump($changes); die();
            }


            $em->detach($entity);

            $entity = $em->find('TreeBundle:Person', $id);

            $changes->setPerson($entity);

            $em->flush();

            $query = $em->createQuery('SELECT p FROM TreeBundle:Person p ORDER BY p.id DESC');

            $course = $query->setMaxResults(1)->getSingleResult();

            if ($course->getFirstName() == $entity->getFirstName()) {
                $em->remove($course);
                $em->flush();
            }

            return $this->redirect($this->generateUrl('thanks', array('id' => $id)));
        }
        return array('entity' => $entity, 'form' => $editForm->createView());
    }

    /**
     * @Route("/suggest/{id}/thanks/", name="thanks")
     * @Method("GET")
     * @Template()
     */
    public function thanksAction($id, Request $request)
    {
        return array('id' => $id);
    }

    /**
     * Displays a form to suggest a new Person entity.
     *
     * @Route("/suggest/new", name="create_suggest_new")
     * @Method("GET")
     * @Template("TreeBundle:Person:new.html.twig")
     */
    public function createNewSuggestAction()
    {
        $entity = new Person();
        $form_info = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form_info' => $form_info->createView(),
        );
    }

    /**
     * Creates a new Person entity.
     *
     * @Route("/suggest/", name="person_suggest")
     * @Method("POST")
     * @Template("TreeBundle:Person:new.html.twig")
     */
    public function suggestPersonAction(Request $request)
    {
        $entity = new Person();

        $form = $this->createSuggestForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $person = $request->get('sandello_familytreebundle_person');
            $request = $request->request->all();

            $entity->setbirthYear("");
            $entity->setdeathYear("");
            $entity->setStatus(true);

            $em->persist($entity);
            $em->flush();
            return $this->redirect($this->generateUrl('person_edit', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form_info' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Person entity.
     *
     * @param Person $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createSuggestForm(Person $entity)
    {
        $form = $this->createForm(new PersonInfoType($this->get('security.context')), $entity, array(
            'attr' => array('class' => 'editinfo'),
            'action' => $this->generateUrl('person_create'),
            'method' => 'POST',
            'em' => $this->getDoctrine()->getManager()
        ));

        $form->add('submit', 'submit', array('label' => 'Save', 'attr' => array('class' => 'grbutton')));

        return $form;
    }


    /**
     * @Route("/suggest/{id}/suggest_avatar/add", name="person_suggest_avatar_add")
     * @Method("POST")
     */
    public function suggestAvatarAddAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->find('TreeBundle:Person', $id);

        $photo_name = $id . '-avatar-' . time() . '.jpg';

        file_put_contents(
            __DIR__ . '/../../../../web' . '/uploads/suggests/' . $photo_name,
            file_get_contents('php://input')
        );

        $changes = new Changes("system", "replace portrait", new \DateTime('NOW'), $entity);
        //$changes->setPerson($entity);

        $change = new FieldChange("avatar", $photo_name, $changes);
        $em->persist($change);
        $em->flush();

        return new Response($photo_name);
    }

    /**
     * @Route("/suggest/{id}/suggest_avatar/remove", name="person_suggest_avatar_remove")
     * @Method("POST")
     */
    public function suggestAvatarRemoveAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->find('TreeBundle:Person', $id);

        $changes = new Changes("system", "portrait incorrect", new \DateTime('NOW'), $entity);
        //$changes->setPerson($entity);

        $change = new FieldChange("avatar", null, $changes);
        $em->persist($change);
        $em->flush();

        return new Response((int)$entity->getSex());
    }

    /**
     * @Route("/suggest/{id}/suggest_photo", name="person_suggest_photo")
     * @Method("POST")
     */
    public function suggestPhotoAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->find('TreeBundle:Person', $id);

        $photo_name = $id . '-' . time() . '.jpg';

        file_put_contents(
            __DIR__ . '/../../../web' . '/uploads/suggests/' . $photo_name,
            file_get_contents('php://input')
        );

        $changes = new Changes("user", "photo", "IT", new \DateTime('NOW'), $entity);
        //$changes->setPerson($entity);

        $change = new FieldChange("photo", $photo_name, $changes);
        $em->persist($change);
        $em->flush();

        return new Response($photo_name);
    }


    /**
     * Creates a form to create a Message entity.
     *
     * @param Message $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createMessageForm(Message $entity)
    {
        $form = $this->createForm(new MessageType(), $entity, array(
            'attr' => array('class' => 'message'),
            'action' => $this->generateUrl('send_message'),
            'method' => 'POST',
            'em' => $this->getDoctrine()->getManager()
        ));

        $form->add('submit', 'submit', array('label' => 'Save', 'attr' => array('class' => 'grbutton')));
        return $form;
    }


    /**
     * @Route("/contacts/", name="contacts")
     */
    public function contactsAction(Request $request)
    {

        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);

        $form->handleRequest($request);
        if ($form->isValid()) {

            $em = $this->getDoctrine()->getManager();

            $message->upload();

            $em->persist($message);
            $em->flush();

            return $this->redirect($this->generateUrl('index'));
        }

        return $this->render('@Tree/Default/contacts.html.twig', [
            'form' => $form->createView()
        ]);

    }


    public function getChangesNumberAction()
    {
        $em = $this->getDoctrine()->getManager();
        $region = $this->get('security.context')->getToken()->getUser()->getRegion();

        if ($region == "all") {
            $entities = $em->getRepository('TreeBundle:Changes')->findAll();
        } else {
            $entities = $em->getRepository('TreeBundle:Changes')->findBy(array("region" => $region));
        }


        $changes_num = count($entities);
        $changes = '';
        if ($changes_num > 0) $changes = '(' . $changes_num . ')';
        return new Response($changes);
    }

    public function getMessagesNumberAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('TreeBundle:Message')->findAll();
        $messages_num = count($entities);
        $messages = '';
        if ($messages_num > 0) $messages = '(' . $messages_num . ')';
        return new Response($messages);
    }

    public function getPersonNumberAction()
    {
        $em = $this->getDoctrine()->getManager();
        $person_num = $em->createQueryBuilder('p')->select('COUNT(p.id)')->from('TreeBundle:Person', 'p')->getQuery()->getSingleScalarResult();
        return new Response($person_num);
    }


}