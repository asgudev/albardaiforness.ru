<?php

namespace FamilyTreeBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SuggestChangesTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/person/1');

        $this->assertTrue($crawler->filter('a:contains("Suggest changes")')->count() > 0);

        $link = $crawler->selectLink('a:contains("Suggest changes")')->link();

        $crawler = $client->click($link);

        // $this->assertTrue($crawler->filter('h1:contains("Suggest changes")')->count() > 0);
    }
}
