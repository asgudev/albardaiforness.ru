<?php

namespace FamilyTreeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table()
 * @ORM\Entity
 */
class Marriage
{
	public function __construct($person = null, $spouse = null)
    {
        $this->person = $person;
        $this->spouse = $spouse;
    }

	/**
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/** 
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="marriages")
     */
    public $person;
	
	/**
     * @ORM\ManyToOne(targetEntity="Person")
     */
    public $spouse;

    /**
     * @var string
     *
     * @ORM\Column(name="marriage_date", type="text", nullable=true)
     */
    private $marriageDate;
	
	/**
     * @var string
     *
     * @ORM\Column(name="divorce_date", type="text", nullable=true)
     */
    private $divorceDate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set marriageDate
     *
     * @param string $marriageDate
     *
     * @return Marriages
     */
    public function setMarriageDate($marriageDate)
    {
        $this->marriageDate = $marriageDate;

        return $this;
    }

    /**
     * Get marriageDate
     *
     * @return string
     */
    public function getMarriageDate()
    {
        return $this->marriageDate;
    }

    /**
     * Set divorceDate
     *
     * @param string $divorceDate
     *
     * @return Marriages
     */
    public function setDivorceDate($divorceDate)
    {
        $this->divorceDate = $divorceDate;

        return $this;
    }

    /**
     * Get divorceDate
     *
     * @return string
     */
    public function getDivorceDate()
    {
        return $this->divorceDate;
    }

    /**
     * Set person
     *
     * @param \FamilyTreeBundle\Entity\Person $person
     *
     * @return Marriages
     */
    public function setPerson(\FamilyTreeBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \FamilyTreeBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set spouse
     *
     * @param \FamilyTreeBundle\Entity\Person $spouse
     *
     * @return Marriages
     */
    public function setSpouse(\FamilyTreeBundle\Entity\Person $spouse = null)
    {
        $this->spouse = $spouse;

        return $this;
    }

    /**
     * Get spouse
     *
     * @return \FamilyTreeBundle\Entity\Person
     */
    public function getSpouse()
    {
        return $this->spouse;
    }
}
