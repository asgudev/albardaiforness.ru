<?php

namespace FamilyTreeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FieldChange
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class FieldChange
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="field", type="string", length=20)
     */
    private $field;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="Changes", inversedBy="changes", cascade={"persist"})
     * @ORM\JoinColumn(name="changes", referencedColumnName="id", nullable=true, onDelete="SET NULL" )
     */
    private $changes;

    function __construct($field = null, $value = null, $changes = null) {
        $this->field = $field;
        $this->value = $value;
        $this->changes = $changes;
        $changes->addChange($this);
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set field
     *
     * @param string $field
     * @return FieldChange
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return string 
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return FieldChange
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue($type = null)
    {
		if (($this->field == "photo") || ($this->field == "avatar") && ($this->value != null))
		{
			if ($type == "url")
			{
				return $this->value;
			} else {
				return "<img src=\"/uploads/suggests/".$this->value."\" class=\"img-responsive profile-photo cbox\">";
			}
		} else {
			return $this->value;
		}
    }

    /**
     * Set changes
     *
     * @param \FamilyTreeBundle\Entity\Changes $changes
     * @return FieldChange
     */
    public function setChanges(\FamilyTreeBundle\Entity\Changes $changes = null)
    {
        $this->changes = $changes;

        return $this;
    }

    /**
     * Get changes
     *
     * @return \FamilyTreeBundle\Entity\Changes 
     */
    public function getChanges()
    {
        return $this->changes;
    }
}
