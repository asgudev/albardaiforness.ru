<?php

namespace FamilyTreeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Person
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FamilyTreeBundle\Entity\Repository\PersonRepository")
 */
class Person
{
    public function returnDate()
    {
        return $this->birthYear;
    }

    public function getNames()
    {
        $lname = $this->lastName ? $this->lastName . ' ' : '';
        $fname = $this->firstName ? $this->firstName . ' ' : '';
        //.($this->birth||$this->death?'(':'').($this->birth?$this->birth->format('Y'):'unknown').($this->death ? ' - ':'').($this->death?$this->death->format('Y'):'unknown').($this->birth||$this->death?')':'')
        $fname = explode(" ", $fname);
        return $lname . $fname[0];
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="firstName", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="lastName", type="string", length=255)
     */
    private $lastName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth", type="date", nullable=true)
     */
    private $birth;

    /**
     * @var string
     *
     * @ORM\Column(name="birthYear", type="string")
     */
    private $birthYear;

    /**
     * @var string
     *
     * @ORM\Column(name="birthDate", type="string", nullable=true)
     */
    private $birthDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="death", type="date", nullable=true)
     */
    private $death;

    /**
     * @var string
     *
     * @ORM\Column(name="deathYear", type="string")
     */
    private $deathYear;

    /**
     * @var string
     *
     * @ORM\Column(name="deathDate", type="string", nullable=true)
     */
    private $deathDate;

    /**
     * @var string
     *
     * @ORM\Column(name="occupation", type="string", length=255, nullable=true)
     */
    private $occupation;


    /**
     * @var string
     *
     * @ORM\Column(name="birthPlace", type="string", length=255, nullable=true)
     */
    private $birthPlace;

    /**
     * @var string
     *
     * @ORM\Column(name="deathPlace", type="string", length=255, nullable=true)
     */
    private $deathPlace;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="privateNote", type="text", nullable=true)
     */
    private $privateNote;

    /**
     * @var string
     * @ORM\ManyToOne(targetEntity="GalleryBundle\Entity\File")
     */
    private $avatarFile;


    /**
     * @ORM\Column(name="avatar_url", type="string", nullable=true)
     */
    private $avatarUrl;

    /**
     * @ORM\ManyToMany(targetEntity="Person", mappedBy="children", cascade={"persist"})
     * @ORM\OrderBy({"sex" = "DESC"})
     */
    private $parents;

    /**
     * @ORM\OneToMany(targetEntity="Marriage", mappedBy="person")
     */
    private $marriages;

    /**
     * @ORM\ManyToMany(targetEntity="Person", inversedBy="parents", cascade={"persist"})
     * @ORM\JoinTable(name="Children",
     * joinColumns={@ORM\JoinColumn(name="child", referencedColumnName="id")},
     * inverseJoinColumns={@ORM\JoinColumn(name="parent", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"birthYear" = "ASC"})
     */
    private $children;

    /**
     * @ORM\ManyToMany(targetEntity="Person", cascade={"persist"})
     * @ORM\JoinTable(name="Brotherhood",
     * joinColumns={@ORM\JoinColumn(name="person_1", referencedColumnName="id")},
     * inverseJoinColumns={@ORM\JoinColumn(name="person_2", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"birthYear" = "ASC"})
     */
    private $brotherhood;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sex", type="boolean", nullable=true)
     */
    private $sex;

    /**
     * @var string
     *
     * @ORM\Column(name="nickname", type="string", length=255, nullable=true)
     */
    private $nickname;

    /**
     * @var string
     *
     * @ORM\Column(name="is_published", type="boolean", options={"default" = true})
     */
    private $isPublished;

    /**
     * @ORM\OneToMany(targetEntity="GalleryBundle\Entity\File", mappedBy="owner", cascade={"remove","persist"})
     */
    private $files;

    /**
     * @ORM\OneToMany(targetEntity="Changes", mappedBy="person", cascade={"remove","persist"})
     */
    private $changes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->marriages = new \Doctrine\Common\Collections\ArrayCollection();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->parents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->brotherhood = new \Doctrine\Common\Collections\ArrayCollection();
        $this->isPublished = true;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Person
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Person
     */
    public function setFirstName($firstName)
    {
        $this->firstName = strtolower($firstName);

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Person
     */
    public function setLastName($lastName)
    {
        $this->lastName = strtolower($lastName);

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set birth
     *
     * @param \DateTime $birth
     * @return Person
     */
    public function setBirth($birth)
    {
        $this->birth = $birth;

        return $this;
    }

    /**
     * Get birth
     *
     * @return \DateTime
     */
    public function getBirth()
    {
        $year = $this->birthYear;
        $date = $this->birthDate;
        if ($date != "") {
            $date = $date . "/";
        }
        return $date . $year;
    }


    /**
     * Set birthAlt
     *
     * @param string $birthAlt
     * @return Person
     */
    public function setBirthAlt($birthAlt)
    {
        if (strlen($birthAlt) == 4) {
            $this->birthYear = $birthAlt;
            $this->birthDate = '';
        } elseif (strlen($birthAlt) == 10) {
            $delimeter = preg_replace("/[0-9\s]/", "", $birthAlt);
            $datearr = explode($delimeter[0], $birthAlt);
            $birthAlt = $datearr[2] . "-" . $datearr[1] . "-" . $datearr[0];
            $this->birthYear = $datearr[2];
            $this->birthDate = $datearr[0] . "/" . $datearr[1];
        } elseif (strlen($birthAlt) == 0) {
            $this->birthYear = '';
            $this->birthDate = '';
        }
        return $this;
    }

    /**
     * Set deathAlt
     *
     * @param string $deathAlt
     * @return Person
     */
    public function setDeathAlt($deathAlt)
    {

        if (strlen($deathAlt) == 4) {
            $this->deathYear = $deathAlt;
            $this->deathDate = '';

        } elseif (strlen($deathAlt) == 10) {
            $delimeter = preg_replace("/[0-9\s]/", "", $deathAlt);
            $datearr = explode($delimeter[0], $deathAlt);
            $deathAlt = $datearr[2] . "-" . $datearr[1] . "-" . $datearr[0];
            $this->deathYear = $datearr[2];
            $this->deathDate = $datearr[0] . "/" . $datearr[1];
        } elseif (strlen($deathAlt) == 0) {
            $this->deathYear = '';
            $this->deathDate = '';
        }
        return $this;
    }

    /**
     * Get birthAlt
     *
     * @return string
     */
    public function getBirthAlt()
    {
        $year = $this->birthYear;
        $date = $this->birthDate;
        if ($date != "") {
            $date = $date . "/";

        }
        return $date . $year;
    }

    /**
     * Get deathAlt
     *
     * @return string
     */
    public function getDeathAlt()
    {
        $year = $this->deathYear;
        $date = $this->deathDate;
        if ($date != "") {
            $date .= "/";

        }
        return $date . $year;
    }

    /**
     * Set death
     *
     * @param \DateTime $death
     * @return Person
     */
    public function setDeath($death)
    {
        $this->death = $death;

        return $this;
    }

    /**
     * Get death
     *
     * @return \DateTime
     */
    public function getDeath()
    {
        $year = $this->deathYear;
        $date = $this->deathDate;
        if ($date != "") {
            $date .= "/";

        }
        return $date . $year;
    }

    /**
     * @return integer
     */
    public function getAge()
    {
        if ($this->birth) {
            $byear = $this->birth->format('Y');
        } elseif ($this->birthYear) {
            $byear = $this->birthYear;
        } else return null;

        if ($this->death) {
            $dyear = $this->death->format('Y');
        } elseif ($this->deathYear) {
            $dyear = $this->deathYear;
        } else return null;


        $interval = $dyear - $byear;
        if ($interval > 120) $interval = null;

        return $interval;
    }

    /**
     * @return string
     */
    public function getDates()
    {
        $dates = "";
        if (($this->birthYear)) {
            $dates = $this->birthYear;
        }

        if (($this->deathYear)) {
            $dates .= " - " . $this->deathYear;
        }

        return $dates;
    }

    /**
     * @return string
     */
    public function getBirthSort()
    {
        if (($this->birth)) {
            $birthsort = $this->birth->format('Ymd');
        }

        return $birthsort;
    }

    /**
     * @return string
     */
    public function getDeathSort()
    {
        if (($this->death)) {
            $deathsort = $this->death->format('Ymd');
        }

        return $deathsort;
    }

    /**
     * Set occupation
     *
     * @param string $occupation
     * @return Person
     */
    public function setOccupation($occupation)
    {
        $this->occupation = strtolower($occupation);

        return $this;
    }

    /**
     * Get occupation
     *
     * @return string
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * Set birthPlace
     *
     * @param string $birthPlace
     * @return Person
     */
    public function setBirthPlace($birthPlace)
    {
        $this->birthPlace = strtolower($birthPlace);

        return $this;
    }

    /**
     * Get birthPlace
     *
     * @return string
     */
    public function getBirthPlace()
    {
        return $this->birthPlace;
    }

    /**
     * Set deathPlace
     *
     * @param string $deathPlace
     * @return Person
     */
    public function setDeathPlace($deathPlace)
    {
        $this->deathPlace = strtolower($deathPlace);

        return $this;
    }

    /**
     * Get deathPlace
     *
     * @return string
     */
    public function getDeathPlace()
    {
        return $this->deathPlace;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Person
     */
    public function setNote($note)
    {
        $this->note = strtolower($note);

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set privateNote
     *
     * @param string $privateNote
     * @return Person
     */
    public function setPrivateNote($privateNote)
    {
        $this->privateNote = strtolower($privateNote);

        return $this;
    }

    /**
     * Get privateNote
     *
     * @return string
     */
    public function getPrivateNote()
    {
        return $this->privateNote;
    }

    /**
     * Set sex
     *
     * @param boolean $sex
     * @return Person
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return boolean
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set nickname
     *
     * @param string $nickname
     * @return Person
     */
    public function setNickname($nickname)
    {
        $this->nickname = strtolower($nickname);

        return $this;
    }

    /**
     * Get nickname
     *
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * Add marriages
     *
     * @param \FamilyTreeBundle\Entity\Marriage $marriages
     * @return Person
     */
    public function addMarriage(\FamilyTreeBundle\Entity\Marriage $marriages)
    {
        $this->marriages[] = $marriages;
        error_log('added spouse to ' . $this->id);

        return $this;
    }

    /**
     * Remove spouse
     *
     * @param \FamilyTreeBundle\Entity\Person $spouse
     */
    public function removeMarriage(\FamilyTreeBundle\Entity\Person $spouse)
    {
        foreach ($this->getMarriages()->getValues() as $marriage) {
            if ($marriage->person_2 == $spouse) {
                $this->marriages->removeElement($marriage);
                error_log('removed spouse from ' . $this->getId());
            }

        }

    }

    /**
     * Get marriages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMarriages()
    {
        return $this->marriages;
    }

    /**
     * Add child
     *
     * @param \FamilyTreeBundle\Entity\Person $child
     * @return Person
     */
    public function addChild(\FamilyTreeBundle\Entity\Person $child)
    {
        if (!in_array($child, $this->children->toArray())) {
            $this->children[] = $child;
            error_log('added child ' . $child->id . ' to ' . $this->id);

            foreach ($this->children as $brother) {
                $brother->addBrotherhood($child);
            }
            $child->addParent($this);
        }

        return $this;
    }

    /**
     * Remove child
     *
     * @param \FamilyTreeBundle\Entity\Person $child
     */
    public function removeChild(\FamilyTreeBundle\Entity\Person $child)
    {
        if (in_array($child, $this->children->toArray())) {
            $this->children->removeElement($child);
            error_log('removed child ' . $child->id . ' from ' . $this->id);

            foreach ($this->children as $brother) {
                $brother->removeBrotherhood($child);
            }
            $child->removeParent($this);
        }

    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {

        return $this->children;
    }

    /**
     * Add parent
     *
     * @param \FamilyTreeBundle\Entity\Person $parent
     * @return Person
     */
    public function addParent(\FamilyTreeBundle\Entity\Person $parent)
    {
        if (!in_array($parent, $this->parents->toArray())) {
            $this->parents[] = $parent;
            error_log('added parent ' . $parent->id . ' to ' . $this->id);
            $parent->addChild($this);
        }
        return $this;
    }

    /**
     * Remove parent
     *
     * @param \FamilyTreeBundle\Entity\Person $parent
     */
    public function removeParent(\FamilyTreeBundle\Entity\Person $parent)
    {
        if (in_array($parent, $this->parents->toArray())) {
            $this->parents->removeElement($parent);
            error_log('removed parent ' . $parent->id . ' from ' . $this->id);

            $parent->removeChild($this);
        }
    }

    /**
     * Get parents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParents()
    {
        return $this->parents;
    }

    /**
     * Add brother
     *
     * @param \FamilyTreeBundle\Entity\Person $brother
     * @return Person
     */
    public function addBrotherhood(\FamilyTreeBundle\Entity\Person $brother)
    {
        if (!in_array($brother, $this->brotherhood->toArray()) && ($this != $brother)) {
            $this->brotherhood[] = $brother;
            error_log('added brother ' . $brother->id . ' to ' . $this->id);
            $brother->addBrotherhood($this);
        }
        return $this;
    }

    /**
     * Remove brotherhood
     *
     * @param \FamilyTreeBundle\Entity\Person $brother
     */
    public function removeBrotherhood(\FamilyTreeBundle\Entity\Person $brother)
    {
        if (in_array($brother, $this->brotherhood->toArray())) {
            $this->brotherhood->removeElement($brother);
            error_log('removed brother ' . $brother->id . ' from ' . $this->id);
            $brother->removeBrotherhood($this);
        }


    }

    /**
     * Get brotherhood
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBrotherhood()
    {
        return $this->brotherhood;
    }

    /**
     * Add brotherhoodInverse
     *
     * @param \FamilyTreeBundle\Entity\Person $brotherhoodInverse
     * @return Person
     */
    public function addBrotherhoodInverse(\FamilyTreeBundle\Entity\Person $brotherhoodInverse)
    {
        $this->brotherhoodInverse[] = $brotherhoodInverse;

        return $this;
    }

    /**
     * Remove brotherhoodInverse
     *
     * @param \FamilyTreeBundle\Entity\Person $brotherhoodInverse
     */
    public function removeBrotherhoodInverse(\FamilyTreeBundle\Entity\Person $brotherhoodInverse)
    {
        $this->brotherhoodInverse->removeElement($brotherhoodInverse);
    }

    /**
     * Get brotherhoodInverse
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBrotherhoodInverse()
    {
        return $this->brotherhoodInverse;
    }


    /**
     * Add files
     *
     * @param \GalleryBundle\Entity\File $file
     * @return Person
     */
    public function addFile(\GalleryBundle\Entity\File $file)
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \GalleryBundle\Entity\File $file
     */
    public function removeFile(\GalleryBundle\Entity\File $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Add changes
     *
     * @param \FamilyTreeBundle\Entity\Changes $changes
     * @return Person
     */
    public function addChange(\FamilyTreeBundle\Entity\Changes $changes)
    {
        $this->changes[] = $changes;

        return $this;
    }

    /**
     * Remove changes
     *
     * @param \FamilyTreeBundle\Entity\Changes $changes
     */
    public function removeChange(\FamilyTreeBundle\Entity\Changes $changes)
    {
        $this->changes->removeElement($changes);
    }

    /**
     * Get changes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChanges()
    {
        return $this->changes;
    }

    /**
     * Set birthYear
     *
     * @param \DateTime $birthYear
     * @return Person
     */
    public function setBirthYear($birthYear)
    {
        $this->birthYear = $birthYear;

        return $this;
    }

    /**
     * Get birthYear
     *
     * @return \DateTime
     */
    public function getBirthYear()
    {
        return $this->birthYear;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     * @return Person
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set deathYear
     *
     * @param \DateTime $deathYear
     * @return Person
     */
    public function setDeathYear($deathYear)
    {
        $this->deathYear = $deathYear;

        return $this;
    }

    /**
     * Get deathYear
     *
     * @return \DateTime
     */
    public function getDeathYear()
    {
        return $this->deathYear;
    }

    /**
     * Set deathDate
     *
     * @param string $deathDate
     * @return Person
     */
    public function setDeathDate($deathDate)
    {
        $this->deathDate = $deathDate;

        return $this;
    }

    /**
     * Get deathDate
     *
     * @return string
     */
    public function getDeathDate()
    {
        return $this->deathDate;
    }

    /**
     * Set avatarUrl
     *
     * @param string $avatarUrl
     *
     * @return Person
     */
    public function setAvatarUrl($avatarUrl)
    {
        $this->avatarUrl = $avatarUrl;



        return $this;
    }

    /**
     * Get avatarUrl
     *
     * @return string
     */
    public function getAvatarUrl()
    {
        if ($this->avatarUrl == null) {
            if ($this->sex == 0) {
                $avatar = "/img/female.png";
            } else {
                $avatar = "/img/male.png";
            }

            if (($this->birthYear == $this->deathYear) || ($this->birthYear == ($this->deathYear - 1))) {
                $avatar = "/img/angel.jpg";
            }
            return $avatar;
        }

        return $this->avatarUrl;

    }

    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     *
     * @return Person
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Set avatarFile
     *
     * @param \GalleryBundle\Entity\File $avatarFile
     *
     * @return Person
     */
    public function setAvatarFile(\GalleryBundle\Entity\File $avatarFile = null)
    {
        $this->avatarFile = $avatarFile;

        return $this;
    }

    /**
     * Get avatarFile
     *
     * @return \GalleryBundle\Entity\File
     */
    public function getAvatarFile()
    {
        return $this->avatarFile;
    }
}
