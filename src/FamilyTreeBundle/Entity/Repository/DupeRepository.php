<?php
/**
 * Created by IntelliJ IDEA.
 * User: iProtoss
 * Date: 30.05.2016
 * Time: 12:25
 */

namespace FamilyTreeBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use FamilyTreeBundle\Entity\Person;

class DupeRepository extends EntityRepository
{

    public function findAll()
    {
        $dupes = $this->createQueryBuilder('d')
            ->where('d.status = 1')
            ->getQuery()
            ->getResult();
        return $dupes;
    }

    public function findDupe(Person $person1, Person $person2)
    {
        $dupe = $this->createQueryBuilder('d')
            ->where('d.person1 = :person1')
            ->andWhere('d.person2 = :person2')
            ->setParameters([
                'person1' => $person1,
                'person2' => $person2,
            ])
            ->getQuery()
            ->getOneOrNullResult();
        return $dupe;
    }

}