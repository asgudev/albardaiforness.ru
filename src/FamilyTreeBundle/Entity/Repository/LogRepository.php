<?php

namespace FamilyTreeBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class LogRepository extends EntityRepository
{
    public function findByPage($page = 0)
    {
        $step = 25;
        //$page = $page < 1 ? 1 : $page;
        $logs = $this->createQueryBuilder('l')
            ->innerJoin('l.person', 'p')
            ->setFirstResult($step*$page)
            ->setMaxResults($step)
            ->orderBy("l.id", "DESC")

            ->getQuery()
            ->getResult();

        return $logs;

    }
}