<?php

namespace FamilyTreeBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use FamilyTreeBundle\Entity\Visitor;

class VisitorRepository extends EntityRepository
{
    public function getVisitorData()
    {
        $qb = $this->createQueryBuilder('v');

        $qb->select('COUNT(v) as visitorSum')
            ->addSelect('SUM(v.pageCount) as pageSum');

        return $qb->getQuery()->getSingleResult();
    }

    public function updateVisitor(Visitor $visitor)
    {
        $qb = $this->createQueryBuilder('v');

        $qb->update()
            ->where('v.id = :id')
            ->set('v.pageCount', $visitor->getPageCount())
            ->set('v.pages', $visitor->getPages())
            ->setParameters([
                'id' => $visitor->getId(),
            ]);

        return $qb->getQuery()->getResult();
    }

}