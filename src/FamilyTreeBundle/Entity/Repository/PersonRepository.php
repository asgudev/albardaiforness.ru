<?php

namespace FamilyTreeBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class PersonRepository extends EntityRepository
{
    public function find($id, $lockMode = null, $lockVersion = null)
    {
        $person = $this->createQueryBuilder('p')
            ->addSelect('pm')
            ->addSelect('pms')
            ->addSelect('pp')
            ->addSelect('pc')
            ->addSelect('pb')
            ->addSelect('pf')
            ->leftJoin('p.marriages', 'pm')
            ->leftJoin('pm.spouse', 'pms')
            ->leftJoin('p.parents', 'pp')
            ->leftJoin('p.children', 'pc')
            ->leftJoin('p.brotherhood', 'pb')
            ->leftJoin('p.files', 'pf')
            ->where('p.id = :id')
            ->setParameters([
                'id' => $id
            ])
            ->getQuery()
            ->getSingleResult();
        return $person;
    }

    public function findDupes($page = 0)
    {
        $step = 300;
        $sql = 'SELECT p1.id as p1, p2.id as p2 FROM `Person` p1 INNER JOIN `Person` p2 WHERE (concat_ws(" ", p1.firstName, p1.lastName, p1.birthYear) = concat_ws(" ", p2.firstName, p2.lastName, p2.birthYear)) AND p1.id <> p2.id AND p1.id >= :start_id AND p1.id <= :end_id ORDER BY p1';

        $query = $this->getEntityManager()->getConnection()->executeQuery($sql, [
            'start_id' => $page * $step,
            'end_id' => ($page + 1) * $step
        ]);


        return $query->fetchAll();
    }

    public function findByIds($ids = [])
    {
        $persons = $this->createQueryBuilder('p')
            ->where('p.id IN (:ids)')
            ->setParameters([
                'ids' => $ids
            ])
            ->getQuery()
            ->getResult();
        return $persons;
    }

    public function findByQuery($query = '')
    {
        $qb = $this->createQueryBuilder('p');
        $result = $qb
            ->orWhere($qb->expr()->eq('p.id', ":query_id"))
            ->orWhere($qb->expr()->like(
                $qb->expr()->concat('p.firstName',$qb->expr()->concat( 'p.lastName', 'p.firstName')), ':query'))
            ->setParameters([
                'query' => '%' . str_replace(' ', '%', $query) . '%',
                'query_id' => $query,
            ])
            ->getQuery()
            ->getResult();
        return $result;
    }


}