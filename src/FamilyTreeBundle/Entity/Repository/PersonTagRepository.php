<?php
/**
 * Created by IntelliJ IDEA.
 * User: iProtoss
 * Date: 25.05.2016
 * Time: 16:15
 */

namespace FamilyTreeBundle\Entity\Repository;


use Doctrine\ORM\EntityRepository;
use FamilyTreeBundle\Entity\Person;
use GalleryBundle\Entity\File;

class PersonTagRepository extends EntityRepository
{
    function findAvatarData(Person $person)
    {
        $tag = $this->createQueryBuilder('pt')
            ->where('pt.person = :person')
            ->andWhere('pt.file = :file')
            ->setParameters([
                'person' => $person,
                'file' => $person->getAvatarFile()
            ])
            ->getQuery()
            ->getOneOrNullResult();

        return $tag;

    }
}