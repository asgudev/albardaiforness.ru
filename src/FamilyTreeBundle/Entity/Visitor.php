<?php
namespace FamilyTreeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="FamilyTreeBundle\Entity\Repository\VisitorRepository")
 * @ORM\Table(name="Visitors")
 */
class Visitor
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="token", type="string", nullable=false)
     */
    private $token;

    /**
     * @ORM\Column(name="referrer", type="string", nullable=true)
     */
    private $referrer;

    /**
     * @ORM\Column(name="ip", type="string", nullable=true)
     */
    private $ip;

    /**
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;


    /**
     * @ORM\Column(name="pages", type="array", nullable=true)
     */
    private $pages;

    /**
     * @ORM\Column(name="page_count", type="integer", nullable=true)
     */
    private $pageCount;


    public function __construct($request)
    {
        $this->token = $request->getSession()->getId();
        $this->referrer = $request->headers->get('referer');
        $this->ip = $request->getClientIp();


        $this->date = new \DateTime();
        $this->pageCount = 0;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @return integer
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Visitor
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }


    /**
     * Set referrer
     *
     * @param string $referrer
     * @return Visitor
     */
    public function setReferrer($referrer)
    {
        $this->referrer = $referrer;

        return $this;
    }

    /**
     * Get referrer
     *
     * @return string
     */
    public function getReferrer()
    {
        return $this->referrer;
    }

    /**
     * Set ip
     *
     * @param string $ip
     * @return Visitor
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Visitor
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set pageCount
     *
     * @param integer $pageCount
     *
     * @return Visitor
     */
    public function setPageCount($pageCount)
    {
        $this->pageCount = $pageCount;

        return $this;
    }

    /**
     * Get pageCount
     *
     * @return integer
     */
    public function getPageCount()
    {
        return $this->pageCount;
    }


    public function addPage()
    {
        $this->pageCount += 1;
    }

    /**
     * Set pages
     *
     * @param array $pages
     *
     * @return Visitor
     */
    public function setPages($pages)
    {
        $this->pages = $pages;

        return $this;
    }

    /**
     * Get pages
     *
     * @return array
     */
    public function getPages()
    {
        return $this->pages;
    }
}
