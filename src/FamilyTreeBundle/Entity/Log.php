<?php

namespace FamilyTreeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use UserBundle\Entity\User;

/**
 * Log
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FamilyTreeBundle\Entity\Repository\LogRepository")
 */
class Log
{
	/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
	
	/**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user", referencedColumnName="id", nullable=true, onDelete="SET NULL" )
     */
    private $user;
	
    /**
     * @ORM\ManyToOne(targetEntity="Person")
     * @ORM\JoinColumn(name="person", referencedColumnName="id", nullable=true, onDelete="SET NULL" )
     */
    private $person;
	
	/**
     * @var string
     *
     * @ORM\Column(name="field", type="string", nullable=true)
     */
    private $field;
	
	/**
     * @var string
     *
     * @ORM\Column(name="action", type="string", nullable=true)
     */
    private $action;
	
	/**
     * @var string
     *
     * @ORM\Column(name="valueold", type="string", nullable=true)
     */
    private $valueold;
	
	/**
     * @var string
     *
     * @ORM\Column(name="valuenew", type="string", nullable=true)
     */
    private $valuenew;
	
	/**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;
	
	public function __construct($user, $person, $action, $field, $valueold, $valuenew)
	{
		$this->user = $user;
		$this->person = $person;

		$this->action = $action;
		$this->field = $field;
		
		switch ($this->field) {
			case "sex":
				$gender = array( "1" => "male", "0" => "female" );
				$this->valuenew = $gender[$valuenew];
				$this->valueold = $gender[$valueold];
				break;
			default:
				$this->valueold = $valueold;
				$this->valuenew = $valuenew;
				break;
		}	
		
		$this->date = new \DateTime();
	}
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set field
     *
     * @param string $field
     * @return Log
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return string 
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return Log
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set valueold
     *
     * @param string $valueold
     * @return Log
     */
    public function setValueold($valueold)
    {
        $this->valueold = $valueold;

        return $this;
    }

    /**
     * Get valueold
     *
     * @return string 
     */
    public function getValueold()
    {
        return $this->valueold;
    }

    /**
     * Set valuenew
     *
     * @param string $valuenew
     * @return Log
     */
    public function setValuenew($valuenew)
    {
        $this->valuenew = $valuenew;

        return $this;
    }

    /**
     * Get valuenew
     *
     * @return string 
     */
    public function getValuenew()
    {
        return $this->valuenew;
    }

    /**
     * Set user
     *
     * @param User $user
     * @return Log
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set person
     *
     * @param \FamilyTreeBundle\Entity\Person $person
     * @return Log
     */
    public function setPerson(\FamilyTreeBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \FamilyTreeBundle\Entity\Person 
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Log
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    public function getLog()
    {
        
        $string = "";
        $old_value = "";
        $action = $this->action;
        if (is_object($this->person))
        {
            $person = "<b><a href=\"/person/".$this->person->getId()."\">".$this->person."</a></b>";
        } else {
            $person = $this->person;
        }
        $field = "<b>".$this->field."</b>";

        $prep = [
            "add" => "to",
            "upload" => "to",
            "remove" => "from",
            "delete" => "from",
        ];

        switch ($this->field) {
            case "sex":
                $gender = array( "1" => "male", "0" => "female" );
                $new_value = $gender[$this->valuenew];
                $old_value = $gender[$this->valueold];
                break;
            case "photo":
                $new_value = "<a href=\"/uploads/photos/".$this->valuenew.'"><img src="/uploads/photos/'.$this->valuenew."\" class=\"img-responsive profile-photo cbox\"></a>";
                break;
            default:
                if ($action == "update") {
                    $old_value = " <br>from <b><i>".$this->valueold."</i></b>";
                    $new_value = " to <b><i>".$this->valuenew."</i></b>";
                } elseif ($action == "set") {
                    $new_value = ": <i>".$this->valuenew."</i>";
                } else {
                    $old_value = ": <i>".$this->valueold."</i>";
                    $new_value = " <i>".$this->valuenew."</i>";
                }
                break;
        }


        return $action." ".$prep[$action]." ".$person." profile ".$field."".$old_value."".$new_value;
    }
}
