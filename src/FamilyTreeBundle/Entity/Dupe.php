<?php

namespace FamilyTreeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Person
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FamilyTreeBundle\Entity\Repository\DupeRepository")
 */
class Dupe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="FamilyTreeBundle\Entity\Person")
     */
    private $person1;

    /**
     * @ORM\ManyToOne(targetEntity="FamilyTreeBundle\Entity\Person")
     */
    private $person2;

    /**
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;


    public function __construct()
    {
        $this->status = true;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Dupe
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set person1
     *
     * @param \FamilyTreeBundle\Entity\Person $person1
     *
     * @return Dupe
     */
    public function setPerson1(\FamilyTreeBundle\Entity\Person $person1 = null)
    {
        $this->person1 = $person1;

        return $this;
    }

    /**
     * Get person1
     *
     * @return \FamilyTreeBundle\Entity\Person
     */
    public function getPerson1()
    {
        return $this->person1;
    }

    /**
     * Set person2
     *
     * @param \FamilyTreeBundle\Entity\Person $person2
     *
     * @return Dupe
     */
    public function setPerson2(\FamilyTreeBundle\Entity\Person $person2 = null)
    {
        $this->person2 = $person2;

        return $this;
    }

    /**
     * Get person2
     *
     * @return \FamilyTreeBundle\Entity\Person
     */
    public function getPerson2()
    {
        return $this->person2;
    }
}
