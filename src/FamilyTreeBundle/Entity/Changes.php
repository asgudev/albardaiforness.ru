<?php

namespace FamilyTreeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Changes
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Changes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text")
     */
    private $comment;
    
    /**
     * @var string
     *
     * @ORM\Column(name="region", type="text")
     */
    private $region;
	
	/**
     * @var string
     *
     * @ORM\Column(name="date", type="date")
     */
    private $pdate;

    /**
     * @ORM\OneToMany(targetEntity="FieldChange", mappedBy="changes", cascade={"remove","persist"})
     * @var ArrayCollection $changes
     */
    private $changes;

    /**
     * @ORM\ManyToOne(targetEntity="Person", inversedBy="changes")
     * @ORM\JoinColumn(name="person", referencedColumnName="id", nullable=true, onDelete="SET NULL" )
     */
    private $person;

    /**
     * Constructor
     */
    public function __construct($author = null, $comment = null, $region = null, $pdate = null, $person = null)
    {
        $this->author = $author;
        $this->comment = $comment;
        $this->region = $region;
        $this->pdate = $pdate;
		$this->person = $person;
        $this->changes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return Changes
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Changes
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
	
	/**
     * Set pdate
     *
     * @param DateTime $pdate
     * @return Changes
     */
    public function setDate($pdate)
    {
        $this->pdate = $pdate;

        return $this;
    }

    /**
     * Get pdate
     *
     * @return DateTime 
     */
    public function getDate()
    {
        return $this->pdate;
    }

    /**
     * Add changes
     *
     * @param \FamilyTreeBundle\Entity\FieldChange $changes
     * @return Changes
     */
    public function addChange(\FamilyTreeBundle\Entity\FieldChange $changes)
    {
        $this->changes[] = $changes;

        return $this;
    }

    /**
     * Remove changes
     *
     * @param \FamilyTreeBundle\Entity\FieldChange $changes
     */
    public function removeChange(\FamilyTreeBundle\Entity\FieldChange $changes)
    {
        $this->changes->removeElement($changes);
    }

    /**
     * Get changes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChanges()
    {
        return $this->changes;
    }

    /**
     * Set person
     *
     * @param \FamilyTreeBundle\Entity\Person $person
     * @return Changes
     */
    public function setPerson(\FamilyTreeBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \FamilyTreeBundle\Entity\Person 
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set pdate
     *
     * @param \DateTime $pdate
     * @return Changes
     */
    public function setPdate($pdate)
    {
        $this->pdate = $pdate;

        return $this;
    }

    /**
     * Get pdate
     *
     * @return \DateTime 
     */
    public function getPdate()
    {
        return $this->pdate;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Changes
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }
}
