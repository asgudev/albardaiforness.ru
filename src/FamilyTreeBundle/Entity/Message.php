<?php

namespace FamilyTreeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Message
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Message
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="author", type="string", nullable=true)
     */
    private $author;

    /**
     * @var string
     * @ORM\Column(name="email", type="string", nullable=true)
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(name="message", type="string", nullable=true)
     */
    private $message;

    /**
     * @var \DateTime
     * @ORM\Column(name="post_date", type="date", nullable=true)
     */
    private $pdate;

    /**
     * @Assert\Image(maxSize="10000000")
     */
    public $file;

    /**
     * @var string
     *
     * @ORM\Column(name="filePath", type="string", length=255, nullable=true)
     */
    private $filePath;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return Message
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Message
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return Message
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set pdate
     *
     * @param \DateTime $pdate
     * @return Message
     */
    public function setPdate($pdate)
    {
        $this->pdate = $pdate;

        return $this;
    }

    /**
     * Get pdate
     *
     * @return \DateTime
     */
    public function getPdate()
    {
        return $this->pdate;
    }

    /**
     * Set filePath
     *
     * @param string $filePath
     * @return Message
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * Get filePath
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    public function getFileview()
    {
        if ($this->filePath != null) {
            $allowed_image = array("jpg", "png", "gif", "bmp", "jpeg");

            if ((in_array(strtolower(substr($this->filePath, -3)), $allowed_image)) || (in_array(strtolower(substr($this->filePath, -4)), $allowed_image))) {
                return "<br><a href=\"/" . $this->getWebPath() . "\" download><img src=\"/" . $this->getWebPath() . "\" class=\"img-responsive profile-photo cbox\"></a>";
            } else {
                return "<br><a href=\"/" . $this->getWebPath() . "\" download>file</a>";
            }
        }
    }

    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        $filename = time() . "." . $this->getFile()->guessExtension();

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        $this->filePath = $filename;

        $this->file = null;
    }

    public function getAbsolutePath()
    {
        return null === $this->filePath
            ? null
            : $this->getUploadRootDir() . '/' . $this->filePath;
    }

    public function getWebPath()
    {
        return null === $this->filePath
            ? null
            : $this->getUploadDir() . '/' . $this->filePath;
    }

    protected function getUploadRootDir()
    {
        return __DIR__ . '/../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/messages';
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

}
