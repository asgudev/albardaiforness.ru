<?php

namespace FamilyTreeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use GalleryBundle\Entity\File;


/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="FamilyTreeBundle\Entity\Repository\PersonTagRepository")
 */
class PersonTag
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="GalleryBundle\Entity\File", inversedBy="personTags")
     */
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity="FamilyTreeBundle\Entity\Person")
     */
    private $person;

    /**
     * @ORM\Column(name="meta", type="array", nullable=true)
     */
    private $meta;

    public function __construct(File $file, Person $person)
    {
        $this->file = $file;
        $this->person = $person;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set meta
     *
     * @param string $meta
     *
     * @return Filetag
     */
    public function setMeta($meta)
    {
        $this->meta = $meta;

        return $this;
    }

    /**
     * Get meta
     *
     * @return string
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Set file
     *
     * @param \FamilyTreeBundle\Entity\File $file
     *
     * @return Filetag
     */
    public function setFile(\FamilyTreeBundle\Entity\File $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return \FamilyTreeBundle\Entity\File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set person
     *
     * @param \FamilyTreeBundle\Entity\Person $person
     *
     * @return Filetag
     */
    public function setPerson(\FamilyTreeBundle\Entity\Person $person = null)
    {
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \FamilyTreeBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }
}
