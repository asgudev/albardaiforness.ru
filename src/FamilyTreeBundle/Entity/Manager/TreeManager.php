<?php

namespace FamilyTreeBundle\Entity\Manager;

use Doctrine\ORM\EntityManager;
use FamilyTreeBundle\Entity\Person;

class TreeManager
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    protected $spousesRoot = [];

    protected $person = [];

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getFamilyTree($entity, $depth = 1)
    {
        $this->person[] = $this->getPerson($entity);
        $this->getParentsRoot($entity, $depth);
        $this->getChildrenRoot($entity, $depth);
        $this->getSpousesRoot($entity);

        $fscript = '';
        foreach ($this->person as $fperson) $fscript .= implode("\t", $fperson) . "\n";
        $fscript_json = array(
            "t" => $fscript
        );

        return json_encode($fscript_json);
    }

    private function getRelativesTop(Person $entity, &$parents)
    {
        foreach ($entity->getParents() as $parent) {
            $parents[] = $parent;
            $this->getRelativesTop($parent, $parents);
        }
    }

    private function getRelativesBottom(Person $entity, &$parents)
    {
        foreach ($entity->getChildren() as $child) {
            $parents[] = $child;
            $this->getRelativesBottom($child, $parents);
        }
    }

    public function getPersonById($id)
    {
        $person = $this->em->getRepository('TreeBundle:Person')->find($id);

        return $person->getNames();
    }


    private function getParentsRoot(Person $entity, $depth = 1)
    {
        foreach ($entity->getParents() as $parent) {
            $person = $this->getPerson($parent);
            if (!in_array($person, $this->person)) $this->person[] = $person;
            if ($depth > 0) {
                $this->getParentsRoot($parent, $depth - 1);
                $this->getChildrenRoot($parent, $depth - 1);
            }
        }
        return $this->person;
    }


    private function getSpousesRoot($entity)
    {
        $spouses = array_unique($this->spousesRoot);
        foreach ($spouses as $spouseID) {
            $entity = $this->em->getRepository('TreeBundle:Person')->find($spouseID);

            $person = $this->getPerson($entity);
            if ((!in_array($person, $this->person))) $this->person[] = $person;
        }

        return $this->person;
    }

    private function getChildrenRoot(Person $entity, $depth = 1)
    {
        foreach ($entity->getChildren() as $child) {
            $person = $this->getPerson($child);
            if ((!in_array($person, $this->person))) $this->person[] = $person;
            if ($depth > 0) {
                $this->getChildrenRoot($child, $depth - 1);
            }
        }
        return $this->person;
    }

    private function getFather(Person $entity)
    {
        $father = null;
        if (count($entity->getParents()) > 0) {
            foreach ($entity->getParents() as $parent) {
                if ($parent->getSex() == 1) {
                    $father = $parent;
                }
            }
        }
        return $father;

    }

    private function getMother(Person $entity)
    {
        $mother = null;
        if (count($entity->getParents()) > 0) {
            foreach ($entity->getParents() as $parent) {
                if ($parent->getSex() == 0) {
                    $mother = $parent;
                }
            }
        }
        return $mother;
    }

    private function getSpouse(Person $entity)
    {
        $marriages = $entity->getMarriages();
        $spouse = null;
        foreach ($marriages as $marriage) {
            $spouse = $marriage->getSpouse();
            $this->spousesRoot[] = $spouse->getId();
        }

        return $spouse;

    }

    private function getPerson(Person $entity)
    {
        if (!$entity) {
            return null;
        }

        if ($entity->getBirth() != "") {
            $birth = $entity->getBirthYear();
        } else {
            $birth = "";
        }
        if ($entity->getDeath() != "") {
            $death = $entity->getDeathYear();
        } else {
            $death = "";
        }

        $person = array("id" => 'i' . $entity->getId(),
            "name" => 'p' . $entity->getFirstName(),
            "lastname" => 'l' . $entity->getLastName(),

            "birth" => 'b' . $birth,
            "death" => 'd' . $death,

            "gender" => 'g' . ($entity->getSex() == 1 ? 'm' : 'f'),
        );

        $father = $this->getFather($entity);
        if ($father) $person["father"] = 'f' . $father->getId();

        $mother = $this->getMother($entity);
        if ($mother) $person["mother"] = 'm' . $mother->getId();

        $photourl = $entity->getAvatarUrl();
        $person["photo"] = 'r' . $photourl;

        $spouse = $this->getSpouse($entity);
        if ($spouse) $person["spouse"] = 's' . $spouse->getId();

        return $person;
    }

}