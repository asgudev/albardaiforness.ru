<?php

namespace FamilyTreeBundle\Entity\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use FamilyTreeBundle\Entity\Person;
use FamilyTreeBundle\Entity\Visitor;
use Symfony\Component\HttpFoundation\RequestStack;


class VisitorManager
{
    /**
     * @var EntityManager
     */
    protected $em;


    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }


    public function save($visitor)
    {
        $this->em->persist($visitor);
        $this->em->flush();
    }


    public function saveOrUpdate(Visitor $visitor)
    {


        if ($visitor->getId()) {
            $this->em->merge($visitor);
        } else {
            $this->em->persist($visitor);
        }
        $this->em->flush();

    }


}