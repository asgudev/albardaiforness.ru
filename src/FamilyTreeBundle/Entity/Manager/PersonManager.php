<?php

namespace FamilyTreeBundle\Entity\Manager;

use Doctrine\ORM\EntityManager;

use FamilyTreeBundle\Entity\Person;
use FamilyTreeBundle\Entity\Marriage;

class PersonManager
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function addParent(Person $person, Person $parent)
    {
        $person->addParent($parent);
        $this->em->persist($person);
        $this->em->flush();
    }

    public function addSpouse(Person $person, Person $spouse)
    {
        $marriage = new Marriage($person, $spouse);
        $marriageInverse = new Marriage($spouse, $person);

        $this->em->persist($marriage);
        $this->em->persist($marriageInverse);
        $this->em->flush();
    }

    public function addChild(Person $person, Person $child)
    {
        $person->addChild($child);

        $this->em->persist($person);
        $this->em->flush();
    }

    public function addBrother(Person $person, Person $brother)
    {
        $person->addBrotherhood($brother);

        $this->em->persist($person);
        $this->em->flush();
    }


    public function removeParent(Person $person, Person $parent)
    {
        $person->removeParent($parent);
        $this->em->persist($person);
        $this->em->flush();
    }

    public function removeSpouse(Person $person, Person $spouse)
    {
        $marriage = $this->em->getRepository("TreeBundle:Marriage")->findOneBy([
            'person' => $person,
            'spouse' => $spouse
        ]);


        $marriageInverse = $this->em->getRepository("TreeBundle:Marriage")->findOneBy([
            'person' => $spouse,
            'spouse' => $person
        ]);

        if ($marriage) $this->em->remove($marriage);
        if ($marriageInverse) $this->em->remove($marriageInverse);
        
        $this->em->flush();
    }

    public function removeChild(Person $person, Person $child)
    {
        $person->removeChild($child);

        $this->em->persist($person);
        $this->em->flush();
    }

    public function removeBrother(Person $person, Person $brother)
    {
        $person->removeBrotherhood($brother);

        $this->em->persist($person);
        $this->em->flush();
    }
}