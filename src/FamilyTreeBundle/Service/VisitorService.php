<?php

namespace FamilyTreeBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use FamilyTreeBundle\Entity\Person;
use FamilyTreeBundle\Entity\Visitor;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class VisitorService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    protected $request;

    /**
     * @var Container
     */
    protected $container;

    /**
     * @param EntityManager $em
     * @param RequestStack $requestStack
     */
    public function __construct(EntityManager $em, Container $container, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
        $this->request = $requestStack->getCurrentRequest();
        $this->container = $container;
    }

    public function init()
    {
        $session = $this->request->getSession();


        if (!$session->get('visitor')) {
            $visitor = new Visitor($this->request);
        } else {
            $visitor = $session->get('visitor');
        }

        $pages = $this->getPages($session);
        $visitor->setPages($pages);
        $visitor->setPageCount(count($pages));

        $this->container->get('tree.visitor.manager')->saveOrUpdate($visitor);


        $session->set('visitor', $visitor);
    }

    public function getPages(SessionInterface $session)
    {
        if (!$session->get('pages')) {
            $pages = [];
            $session->set('pages', $pages);
        } else {
            $pages = $session->get('pages');
        }

        $currentPage = $this->request->getRequestUri();
        if (!in_array($currentPage, $pages)) $pages[] = $currentPage;

        $session->set('pages', $pages);

        return $pages;
    }

}