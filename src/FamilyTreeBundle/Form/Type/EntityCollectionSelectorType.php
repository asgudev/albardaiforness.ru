<?php
namespace FamilyTreeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use FamilyTreeBundle\Form\DataTransformer\EntityCollectionToIdTransformer;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EntityCollectionSelectorType extends AbstractType
{
    /**
     * @var ObjectManager
     */
     protected $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new EntityCollectionToIdTransformer($this->om, $options['configs']['entity']);
        $builder->resetViewTransformers();
        $builder->addModelTransformer($transformer);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'invalid_message' => 'The selected entity does not exist',
            'required' => true,
            'auto_initialize' => false,
            'configs' => array('multiple' => true),
            'error_bubbling' => false, 
        ));
    }

    public function getParent()
    {
        return 'genemu_jqueryselect2_hidden';
    }

    public function getName()
    {
        return 'entity_collection_selector';
    }
}