<?php
/**
 * Created by IntelliJ IDEA.
 * User: iProtoss
 * Date: 25.05.2016
 * Time: 14:17
 */

namespace FamilyTreeBundle\Form;

use Elastica\Type\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;

class PortraitType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("file", FileType::class)
            ;
    }
}