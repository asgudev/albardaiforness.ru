<?php
namespace FamilyTreeBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use FamilyTreeBundle\Entity\Person;

class MainSearchTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an firstname
     *
     * @param  Issue|null $issue
     * @return string
     */
    public function transform($firstName)
    {
        return $firstName." ";
    }
    
    public function reverseTransform($firstName)
    {
        return $firstName;
    }

}