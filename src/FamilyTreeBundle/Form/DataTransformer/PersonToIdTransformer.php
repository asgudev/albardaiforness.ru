<?php
namespace FamilyTreeBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use FamilyTreeBundle\Entity\Person;

class PersonToIdTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param  Issue|null $issue
     * @return string
     */
    public function transform($persons)
    {
        if (null === $persons) {
            return "";
        }

        $ids = array();
        foreach ($persons as $person) {
            $ids[] = $person->getId();
        }

        return implode(',', $ids);
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  string $number
     *
     * @return Issue|null
     *
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($ids)
    {
        if(!is_array($ids))
            $ids = explode(',', $ids);

        if (!$ids or !is_array($ids) or count($ids) < 1) {
            return null;
        }

        $persons = $this->om->createQueryBuilder('p')
            ->select('p')
            ->from('TreeBundle:Person','p')
            ->where('p.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult()
        ;

        if (null === $persons or count($persons) < 1) {
            throw new TransformationFailedException(sprintf(
                'The Persons with ids "%s" does not exist!',
                implode(', ', $ids)
            ));
        }

        return $persons;
    }
}