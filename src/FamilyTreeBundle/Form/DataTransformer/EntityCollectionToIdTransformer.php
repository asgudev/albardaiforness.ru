<?php
namespace FamilyTreeBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\ORM\PersistentCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Collections\ArrayCollection;

class EntityCollectionToIdTransformer implements DataTransformerInterface
{
    /**
     * @var ObjectManager
     */
    private $om;

    /**
     * @var string The Doctrine entity type to use
     */
    private $entityType;

    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om, $entityType)
    {
        $this->om = $om;
        $this->entityType = $entityType;
    }

    /**
     * Transforms a collection of entities to a comma separated string
     *
     * @param  ArrayCollection $entities
     * @return string
     */
    public function transform($entities)
    {
        if (null == $entities || empty($entities)) {
            return '';
        }

        $results = '';
        foreach ($entities as $entity) {
            $results .= $entity->getId() . ',';
        }
        $results = trim($results, ' ,');

        return $results;
    }

   /**
    * Transforms a string of comma separated IDs to a PersistentCollection for Doctrine
    *
    * @param  string $values
    * @return PersistentCollection|ArrayCollection
    * @throws TransformationFailedException if entity is not found.
    */
    public function reverseTransform($values)
    {
        if (!$values) {
            return new ArrayCollection();
        }
        $values = explode(',', $values);

        $collection = array();
        foreach ($values as $id) {
            $item = $this->om->getRepository($this->entityType)->findOneById($id);

            if (!is_null($item)) {
                $collection[] = $item;
            }
            else {
                throw new TransformationFailedException(sprintf(
                    'An entity with ID "%s" does not exist!',
                    $value
                ));
            }
        }

        return new PersistentCollection($this->om, $this->entityType, new ArrayCollection($collection));
    }
}