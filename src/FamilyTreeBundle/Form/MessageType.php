<?php

namespace FamilyTreeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use FamilyTreeBundle\Entity\Message;

class MessageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('author', TextType::class, [
                'label' => 'Name',
                'attr' => [
                    'placeholder' => 'Name',
                    'autocomplete' => 'on'
                ]
            ])
            ->add('email', TextType::class, [
                'label' => 'Email',
                'attr' => ['placeholder' => 'Email', 'autocomplete' => 'off']])
            ->add('file', FileType::class, [
                'label' => 'File',
                'required' => false,
                'attr' => [
                    'placeholder' => 'File',
                    'class' => 'file_upload'
                ]
            ])
            ->add('message', TextareaType::class, [
                'label' => 'Message',
                'required' => true,
                'attr' => [
                    'placeholder' => 'Message',
                    'rows' => '5',
                    'autocomplete' => 'off'
                ]
            ]);
    }

    /**
     * @param OptionsResolverInterface $resolver
     */

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
                'data_class' => Message::class
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'contact';
    }
}
