<?php

namespace FamilyTreeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OneLineType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
       /* $builder
            ->add('field', 'choice', 
			array('choices'=>
			array(	'lastName'=>'Surname', 
					'firstName'=>'Name', 
					'nickname'=>'Nickname', 
				 	'birth'=>'Birth date', 
					'death'=>'Death date', 
					'birthPlace'=>'Birth place',
					'deathPlace'=>'Death place',
					'couse'=>'Couse name',
					'parent'=>'Parent name',
					'child'=>'Child name')))

            ->add('value', 'text', array());*/
        
		$builder
            ->add('firstName', null, array('label'=>'Name', 'attr'=>array('autocomplete'=>'on')))
            ->add('lastName', null, array('label'=>'Surname', 'attr'=>array('autocomplete'=>'off')))
            ->add('nickname', null, array('attr'=>array('autocomplete'=>'off')))
            ->add('birthPlace', null, array('attr'=>array('autocomplete'=>'off')))
			->add('deathPlace', null, array('attr'=>array('autocomplete'=>'off')));
    }
	
	/**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildNewForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', null, array('label'=>'Name', 'attr'=>array('autocomplete'=>'on')))
            ->add('lastName', null, array('label'=>'Surname', 'attr'=>array('autocomplete'=>'off')))
            ->add('nickname', null, array('attr'=>array('autocomplete'=>'off')))
            ->add('birthPlace', null, array('attr'=>array('autocomplete'=>'off')))
			->add('deathPlace', null, array('attr'=>array('autocomplete'=>'off')));
    }
	
	
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return null;
    }
}
