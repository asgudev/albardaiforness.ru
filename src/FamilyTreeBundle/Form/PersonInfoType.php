<?php

namespace FamilyTreeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FamilyTreeBundle\Form\DataTransformer\PersonToIdTransformer;
use Symfony\Component\Security\Core\SecurityContext;

class PersonInfoType extends AbstractType
{
    private $securityContext;

    public function __construct(SecurityContext $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => 'Name',
                'attr' => [
                    'autocomplete' => 'on'
                ]
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Surname',
                'attr' => [
                    'autocomplete' => 'off'
                ]
            ])
            ->add('nickname', null, array('attr' => array('autocomplete' => 'off')))
            ->add('birth_alt', null, array('attr' => array('autocomplete' => 'off'), 'label' => 'Birth'))
            ->add('birthPlace', null, array('attr' => array('autocomplete' => 'off')))
            ->add('death_alt', null, array('attr' => array('autocomplete' => 'off'), 'label' => 'Death'))
            ->add('deathPlace', null, array('attr' => array('autocomplete' => 'off')))
            ->add('occupation', null, array('attr' => array('autocomplete' => 'off')))
            ->add('sex', 'choice', array('choices' => array('1' => 'Male', '0' => 'Female'), 'attr' => array('autocomplete' => 'off')))
            ->add('note', null, array('attr' => array('autocomplete' => 'off')))
        ;

        $builder->add('submit', 'submit', array('label' => 'Save', 'attr' => array('class' => 'grbutton')));
        $builder->add('delete', 'submit', array('label' => 'Delete', 'attr' => array('class' => 'grbutton')));

        if ($this->securityContext->isGranted('ROLE_ADMIN')) {
            $builder->add('privateNote', null, array('attr' => array('autocomplete' => 'off')));
        }
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FamilyTreeBundle\Entity\Person',
            'validation_groups' => false,
            'em' => null,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'person_edit';
    }
}
