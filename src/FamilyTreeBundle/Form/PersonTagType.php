<?php

namespace FamilyTreeBundle\Form;

use Doctrine\ORM\EntityRepository;
use FamilyTreeBundle\Entity\Person;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;


class PersonTagType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $em = $options["erep"];
        $options["erep2"] = "test";

        $builder
            ->add('person', HiddenType::class, [

            ])
            ->add('meta', HiddenType::class, [

            ]);

        $builder->get('person')
            ->addModelTransformer(new CallbackTransformer(
                function ($person) use ($em) {
                    return $person->getId();
                },
                function ($person_id) use ($em) {
                    return $em->getRepository("TreeBundle:Person")->find($person_id);
                }
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FamilyTreeBundle\Entity\PersonTag',
            'erep' => null,

        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FamilyTreeBundle\Entity\PersonTag',
            'erep' => null,
        ));
    }
}