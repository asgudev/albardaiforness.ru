<?php

namespace FamilyTreeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FamilyTreeBundle\Form\DataTransformer\MainSearchTransformer;

class MainSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /*$entityManager = $options['em'];
        $transformer = new MainSearchTransformer($entityManager);*/

        $builder
            ->add('id', null, array('label' => 'Id', 'required' => false, 'attr' => array('autocomplete' => 'on')))
            ->add('lastName', null, array('label' => 'Surname', 'required' => false, 'attr' => array('autocomplete' => 'on')))
            ->add('firstName', null, array('label' => 'Name', 'required' => false, 'attr' => array('autocomplete' => 'on')))//->addModelTransformer($transformer)
            ->add('nickname', null, array('label' => 'Nickname', 'required' => false, 'attr' => array('autocomplete' => 'on')))
            ->add('sex', 'choice', array('choices' => array('1' => 'Male', '0' => 'Female'), 'placeholder' => false, 'multiple' => false, 'expanded' => true, 'required' => false, 'attr' => array('autocomplete' => 'on')))
            ->add('birthYear', null, array('label' => 'Birth date', 'required' => false, 'attr' => array('autocomplete' => 'on')))
            ->add('deathYear', null, array('label' => 'Death date', 'required' => false, 'attr' => array('autocomplete' => 'on')))
            ->add('birthPlace', null, array('label' => 'Birth place', 'required' => false, 'attr' => array('autocomplete' => 'on')))
            ->add('deathPlace', null, array('label' => 'Death place', 'required' => false, 'attr' => array('autocomplete' => 'on')))
            ->add('occupation', null, array('label' => 'Occupation', 'required' => false, 'attr' => array('autocomplete' => 'on')))
            ->add('note', null, array('label' => 'Note', 'required' => false, 'attr' => array('autocomplete' => 'on')))
            ->add('parentName', null, array('label' => 'Parent', 'required' => false, 'attr' => array('autocomplete' => 'on')))
            ->add('childName', null, array('label' => 'Child', 'required' => false, 'attr' => array('autocomplete' => 'on')))
            ->add('couseName', null, array('label' => 'Couse', 'required' => false, 'attr' => array('autocomplete' => 'on')))
            ->add('broName', null, array('label' => 'Brother', 'required' => false, 'attr' => array('autocomplete' => 'on')));

        $builder->add('Search', 'submit', array('label' => 'Search', 'attr' => array('class' => 'search-button')));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array());
    }

    /**
     * @return string
     */
    public function getName()
    {
        return null;
    }
}
