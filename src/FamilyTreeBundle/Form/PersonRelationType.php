<?php

namespace FamilyTreeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FamilyTreeBundle\Form\DataTransformer\PersonToIdTransformer;

class PersonRelationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['em'];
        $transformer = new PersonToIdTransformer($entityManager);

        $builder
            ->add(
                $builder->create('parents', 'hidden', array())
                    ->addModelTransformer($transformer)
            )
            ->add(
                $builder->create('marriages', 'hidden', array())
                    ->addModelTransformer($transformer)
            )
            ->add(
                $builder->create('children', 'hidden', array())
                    ->addModelTransformer($transformer)
            )
            ->add(
                $builder->create('brotherhood', 'hidden', array())
                    ->addModelTransformer($transformer)
            )
            /*->add('marriages', 'bootstrap_collection', array(
                'allow_add'          => true,
                'allow_delete'       => true,
                'add_button_text'    => 'Add marriage',
                'delete_button_text' => 'Delete marriage',
                'sub_widget_col'     => 9,
                'button_col'         => 3,
                'type'               => 'entity_collection_selector',
                'options'            => array(
                    'entity' => 'FamilyTreeBundle\Entity\Person'
                )
            ))
            ->add('children', 'bootstrap_collection', array(
                'allow_add'          => true,
                'allow_delete'       => true,
                'add_button_text'    => 'Add child',
                'delete_button_text' => 'Delete child',
                'sub_widget_col'     => 9,
                'button_col'         => 3,
                'type'               => 'entity_collection_selector',
                'options'            => array(
                    // 'class' => 'FamilyTreeBundle\Entity\Person'
                )
            ))
            ->add('parents', 'bootstrap_collection', array(
                'allow_add'          => true,
                'allow_delete'       => true,
                'add_button_text'    => 'Add parent',
                'delete_button_text' => 'Delete parent',
                'sub_widget_col'     => 9,
                'button_col'         => 3,
                'type'               => 'genemu_jqueryselect2_hidden',
                'options'            => array(
                    // 'class' => 'FamilyTreeBundle\Entity\Person'
                )
            ))
            ->add('brotherhood', 'bootstrap_collection', array(
                'allow_add'          => true,
                'allow_delete'       => true,
                'add_button_text'    => 'Add bro|sis',
                'delete_button_text' => 'Delete bro|sis',
                'sub_widget_col'     => 9,
                'button_col'         => 3,
                'type'               => 'genemu_jqueryselect2_hidden',
                'options'            => array(
                    // 'class' => 'FamilyTreeBundle\Entity\Person'
                )
            ))*/
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FamilyTreeBundle\Entity\Person'
        ))
        ->setRequired(array(
            'em',
        ))
        ->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sandello_familytreebundle_person';
    }
}
