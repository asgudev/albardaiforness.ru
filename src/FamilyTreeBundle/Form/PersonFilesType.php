<?php

namespace FamilyTreeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FamilyTreeBundle\Form\DataTransformer\PersonToIdTransformer;

class PersonFilesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['em'];
        $transformer = new PersonToIdTransformer($entityManager);

        $builder
            ->add('photos', 'bootstrap_collection', array(
                    'prototype_name'     => '__photos__',
                    'label'              => 'Photos',
                    'type'               => new \FamilyTreeBundle\Form\PhotoType(),
                    'allow_add'          => true,
                    'allow_delete'       => true,
                    'add_button_text'    => 'Add photo',
                    'delete_button_text' => 'Remove photo',
                    'sub_widget_col'     => 9,
                    'button_col'         => 3
                )
            )
            ->add('files', 'bootstrap_collection', array(
                    'prototype_name'     => '__files__',
                    'label'              => 'Files',
                    'type'               => new \FamilyTreeBundle\Form\FileType(),
                    'allow_add'          => true,
                    'allow_delete'       => true,
                    'add_button_text'    => 'Add photo',
                    'delete_button_text' => 'Remove photo',
                    'sub_widget_col'     => 9,
                    'button_col'         => 3
                )
            )
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FamilyTreeBundle\Entity\Person'
        ))
        ->setRequired(array(
            'em',
        ))
        ->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sandello_familytreebundle_person';
    }
}
