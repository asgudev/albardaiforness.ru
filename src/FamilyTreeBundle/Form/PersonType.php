<?php

namespace FamilyTreeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use FamilyTreeBundle\Form\DataTransformer\PersonToIdTransformer;

class PersonType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['em'];
        $transformer = new PersonToIdTransformer($entityManager);

        $builder
            ->add('firstName', null, array('attr'=>array('autocomplete'=>'off')))
            ->add('lastName', null, array('attr'=>array('autocomplete'=>'off')))
            ->add('nickname', null, array('attr'=>array('autocomplete'=>'off')))
            ->add('birth', null, array('widget'=>'single_text', 'attr'=>array('autocomplete'=>'off')))
            ->add('birthPlace', null, array('attr'=>array('autocomplete'=>'off')))
            ->add('death', null, array('widget'=>'single_text', 'attr'=>array('autocomplete'=>'off')))
            ->add('deathPlace', null, array('attr'=>array('autocomplete'=>'off')))
            ->add('occupation', null, array('attr'=>array('autocomplete'=>'off')))
            ->add('sex', 'choice', array('choices'=>array('1'=>'Male', '0'=>'Female'), 'attr'=>array('autocomplete'=>'off')))
            ->add('note', null, array('attr'=>array('autocomplete'=>'off')))
            ->add('privateNote', null, array('attr'=>array('autocomplete'=>'off')))
			->add('parentsString', 'hidden', array())
			->add('spousesString', 'hidden', array())
			->add('childrenString', 'hidden', array())
			->add('brotherhoodString', 'hidden', array())
			;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'FamilyTreeBundle\Entity\Person',
			'validation_groups' => false,
        ))
        ->setRequired(array(
            'em',
        ))
        ->setAllowedTypes(array(
            'em' => 'Doctrine\Common\Persistence\ObjectManager',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sandello_familytreebundle_person';
    }
}
